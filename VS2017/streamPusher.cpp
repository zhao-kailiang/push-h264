#include "pullStream.h"
#include "pushStream.h"
#include "net/Logger.h"
#include <iostream>
#include <memory>

#include "libhv/include/hv/HttpServer.h"

#pragma comment(lib,"hv.lib")
#pragma comment(lib,"HBPlatSdk.lib")

using namespace std;
using namespace xop;
using namespace hv;
using namespace nlohmann;


int cnt = std::thread::hardware_concurrency();
std::shared_ptr<xop::EventLoop> g_eventLoop(new xop::EventLoop(cnt));
map<std::string, std::unique_ptr<pushStream>> g_pushList;          // 保存推送的流列表
void delSinalSympol(string& info, char sig = '.') {
    string::iterator it = info.begin();
    for (; it != info.end();) {
        if (*it == sig)
            it = info.erase(it);
        else
            it++;
    }
}

int main() {

    xop::Logger::Instance().Init((char*)"tarnsmitTools.log");
    Pull::Ptr pull = make_shared<Pull>();
    auto result = pull->loginPlatform();

    if (!result) {
        LOG_DEBUG("登陆失败");
        return 0;
    }
    hv::Json deviceList = pull->getDeviceList();
    std::shared_ptr<HttpService> router(new HttpService);
    // 获取设备树信息
    router->GET("/getList", [&pull,&deviceList](HttpRequest* req, HttpResponse* resp) {
        std::string deviceId = req->GetParam("update");
        if (!deviceId.empty() && stoi(deviceId) == 1) {
            deviceList = pull->getDeviceList();
        }

        int code = -1;
        if (!deviceList.empty()) {
            code = 200;     
            resp->json["msg"] = deviceList;
        }
        resp->json["code"] = code;
        return 200;
    });

    // 拉流
    // http://10.126.89.30:8080/pullStream?deviceId=13&devPath=05
    router->GET("/pullStream", [&](HttpRequest* req, HttpResponse* resp) {
        std::string deviceId = req->GetParam("deviceId");
        std::string devPath = req->GetParam("devPath");
        std::string keyInfo = deviceId + "-" + devPath;
        std::string pushStreamId = deviceId + devPath;
        delSinalSympol(pushStreamId);
        auto deviceInfo = g_pushList.find(pushStreamId);
        // 已经在推流中
        if (!deviceId.empty() && deviceInfo != g_pushList.end()) {
            resp->json["msg"] = "was pushed...";
            resp->json["code"] = 401;
            return 200;
        }

        auto device = pull->_streamInfoList.find(keyInfo);
        if (device != pull->_streamInfoList.end()) {
            result = pull->pullStreamFromDeviceId(keyInfo);
            if (result == false) {
                LOG_DEBUG("打开设备失败 id:%d", deviceId);
                resp->json["msg"] = "pull stream error";
                resp->json["code"] = 401;
                return 200;
            }
            
        } else {
            resp->json["msg"] = "not found device..." + keyInfo;
            resp->json["code"] = 401;
            return 200;
        }
       
        std::unique_ptr<pushStream> push(new pushStream(g_eventLoop));
        xop::Timer::Sleep(1000 * 3);
        MediaType codeType = device->second->frameCacheList->getFrameType();
        if (codeType == NONE) {
            LOG_DEBUG("获取编码协议失败:%s %d %d", device->first.data(), device->second->streamHandle, codeType);
            pull->closeStreamFromDeviceId(keyInfo);
            resp->json["msg"] = "get codeinfo error";
            resp->json["code"] =401;
            return 200;
        }
        push->startPush(stoi(pushStreamId), device->second->frameCacheList.get(), codeType);
        g_pushList[keyInfo] = std::move(push);
        LOG_DEBUG("添加一路推流:%s %d %d", device->first.data(), device->second->streamHandle, codeType);
        resp->json["msg"] = "push stream success";

        resp->json["code"] = 200;
        return 200;
    });

    router->GET("/closeStream", [&](HttpRequest* req, HttpResponse* resp) {
        std::string deviceId = req->GetParam("deviceId");
        std::string DevPath = req->GetParam("DevPath");
        std::string keyInfo = deviceId + "-" + DevPath;
        std::string pushStreamId = deviceId + DevPath;
        delSinalSympol(pushStreamId);
        auto deviceInfo = g_pushList.find(pushStreamId);
        if (deviceInfo != g_pushList.end()) {
            // 关闭拉流
            int result = pull->closeStreamFromDeviceId(keyInfo);
            if (result)
                LOG_DEBUG("close %s error", keyInfo.data());
            // 清除缓存
            auto device = pull->_streamInfoList.find(keyInfo);
            if (device != pull->_streamInfoList.end()) {
                device->second->frameCacheList->clear();
            }
            // 移除列表
            g_pushList.erase(deviceInfo);
        }
        resp->json["msg"] = "remove" + pushStreamId;
        return  200;
    });

    router->GET("/get", [&](HttpRequest* req, HttpResponse* resp) {
        resp->json["origin"] = req->client_addr.ip;
        resp->json["url"] = req->url;
        resp->json["args"] = req->query_params;
        resp->json["headers"] = req->headers;
        return 200;
    });

    HttpServer server;
    server.registerHttpService(router.get());
    server.setPort(8080);
    server.setThreadNum(4);
    server.run();
    return 0;
}

//int main01(int argc, char** argv) 
//{
//
//    map<std::string, std::unique_ptr<pushStream>> spush;
//    while (true) {
//        LOG_DEBUG("在线设备:%d ", pull->_streamInfoList.size());
//        for (auto& device : pull->_streamInfoList) {
//            if (device.second->channelType != NODETYPE::ICMS_TYPE_DEVICE)
//                continue;
//
//            std::string keyInfo = std::to_string(device.second->deviceId).append("-").append(device.second->sDevPath);
//            
//            if (spush.find(keyInfo) != spush.end())
//                continue;
//             // 测试推送数量限制
//            if (spush.size() == 4)
//                continue;
//            string streamIdStr = std::to_string(device.second->deviceId).append(device.second->sDevPath);
//            delSinalSympol(streamIdStr);
//            int streamId = stoi(streamIdStr);
//            
//            if (streamId == 182 || streamId == 1315) {
//                break;
//            }
//
//            result = pull->pullStreamFromDeviceId(keyInfo);
//            if (result == false) {
//                LOG_DEBUG("打开设备失败 id:%d", device.first);
//                return 0;
//            }
//
//            std::unique_ptr<pushStream> push(new pushStream);
//           
//            push->setRestartCallback([&]() {
//                std::string keyInfo = std::to_string(device.second->deviceId).append("-").append(device.second->sDevPath);
//                int result = pull->closeStreamFromDeviceId(keyInfo);
//                if (result)
//                    LOG_DEBUG("close %s error", keyInfo.data());
//                spush[keyInfo].release();
//                auto push = spush.find(keyInfo);
//                if (push != spush.end()) {
//                    spush.erase(push);
//                }
//
//             });
//            xop::Timer::Sleep(1000);
//            MediaType codeType = device.second->frameCacheList->getFrameType();
//            if (codeType == NONE) {
//                LOG_DEBUG("获取编码协议失败:%s %d %d", device.first.data(), device.second->streamHandle, codeType);
//                pull->closeStreamFromDeviceId(keyInfo);
//                continue;
//            }
//                
//            push->startPush(streamId, device.second->frameCacheList.get(), codeType);
//            spush[keyInfo] = std::move(push);
//            xop::Timer::Sleep(1000 * 1);
//            LOG_DEBUG("添加一路推流:%s %d %d", device.first.data(), device.second->streamHandle, codeType);
//        }
//        xop::Timer::Sleep(1000 * 10);
//    }
//   
//
//
//    return 0;
//}
