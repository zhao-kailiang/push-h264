#pragma once

#include <memory>
#include <string>
#include <list>
#include <mutex>
#include <map>
#include <iostream>
#include <functional>
#include <exception>
#include <mutex>
#include <unordered_map>

#include <Windows.h>
#include <wchar.h>

#include "net/Logger.h"
#include "net/Timestamp.h"
#include "xop/media.h"
#include "xop/H264Source.h"
#include "xop/H264Parser.h"
#include "libhv/include/hv/json.hpp"
#include "libhv/include/hv/http_content.h"
#include "HBPlatSdk.h"

using namespace xop;

class StreamSourceList {
public:
    using Ptr = std::shared_ptr<StreamSourceList>;
    StreamSourceList() = default;
    ~StreamSourceList() = default;

    void pushFrame(xop::AVFrame& frame) {
        
        std::lock_guard<decltype(_rwMtx)> lk(_rwMtx);
        _totalCnt++;
        while (_list.size() >= 60) {
            _list.pop_front();
        }
        // 保证首帧编码类型
        if (_cnt) {
            this->setFrameType(frame.protocal);
            _cnt = false;
            LOG_DEBUG("first frame %d %x", frame.protocal, this);
        }
  
        _list.emplace_back(frame);
    }
    bool getFrame(xop::AVFrame& frame) {
        std::lock_guard<decltype(_rwMtx)> lk(_rwMtx);
        if (_list.empty())
            return false;
        frame = _list.front();
        _list.pop_front();

        return true;
    }

    void setFrameType(MediaType type) {
        _codeType = type;
    }
    MediaType getFrameType() {
        LOG_DEBUG("get first frame %d %x", _codeType, this);
        return  _codeType;
    }

    bool empty() {
        return   _list.empty();
    }

    void clear() {
        _list.clear();
    }

private:
    std::list<xop::AVFrame> _list;   // char *  len ;
    std::recursive_mutex _rwMtx;
    static int _lossCnt;
    static int _totalCnt;
    xop::Timestamp _ts;
    MediaType _codeType = NONE;
    bool _cnt = true;
    FILE* _outFile = nullptr;   // 用于测试保存pull下的数据

};
int StreamSourceList::_lossCnt = 0;
int StreamSourceList::_totalCnt = 0;


void CALLBACK fLoginStatusCallBack(char* sSessionID, long lConErr, LPICMS_LOGININFO pstLogInfo, DWORD dwUser);
void CALLBACK fStreamCallBack(char* sSessionID, long lDeviceId, long lStreamHandle, LPICMS_FRAMEINFO pFrameInfo,
    BYTE* pBuffer, DWORD dwBufSize, DWORD dwUser);
void CALLBACK fDeviceRealPlayStatusCallBack(long plstreamHandle, long lRealPlayErr, char* strDescripton);


class Pull : public std::enable_shared_from_this<Pull> {
public:
    using Ptr = std::shared_ptr<Pull>;
    struct DeviceInfo {
        using Ptr = std::shared_ptr<DeviceInfo>;
        unsigned long deviceId{ 0 };
        long streamHandle{ 0 };
        bool online{ false };
        NODETYPE channelType{ ICMS_TYPE_DEVICE };
        char sDevPath[256];
        StreamSourceList::Ptr frameCacheList;
    };

    std::map<std::string, DeviceInfo::Ptr> _streamInfoList;          // 流id

    Pull() : _login(std::make_shared<ICMS_LOGININFO>()) { loadConfig(); };
    ~Pull() = default;
    
    // 登录平台获取列表
    bool loginPlatform() {
        LOG_DEBUG("ICMS_UserLogin start");
        int loginResult = ICMS_Init();

        loginResult = ICMS_UserLogin((char*)_ip.data(), _port, (char*)_username.data(),
            (char*)_sercret.data(), _login.get());
        if (loginResult) {
            LOG_DEBUG("登录失败:%d", loginResult);
            return false;
        }

        loginResult = ICMS_LoginStatus(_login->sSessionID, fLoginStatusCallBack, (unsigned long)shared_from_this().get());
        if (loginResult)
            LOG_DEBUG("设置登录状态回调失败:%d", loginResult);

        return true;
    }

    // 根据设备id拉流
    bool pullStreamFromDeviceId(std::string id) {           
        LOG_DEBUG("ICMS_RealPlayAsync start %s", id.data());
        ICMS_CLIENTINFO stClientInfo{ "", 0 ,0,nullptr,ICMS_STREAM_REALTIME_PRIORITY ,"" };
        memcpy(stClientInfo.szPath, _streamInfoList[id]->sDevPath, strlen(_streamInfoList[id]->sDevPath) + 1);
        ICMS_GLDATA stGldata3{ "",_streamInfoList[id]->deviceId,_streamInfoList[id]->channelType };
        memcpy(stGldata3.sDevPath, _streamInfoList[id]->sDevPath, strlen(_streamInfoList[id]->sDevPath) + 1);
  
        auto loginResult = ICMS_RealPlayAsync(_login->sSessionID, stGldata3, stClientInfo, fDeviceRealPlayStatusCallBack, 0, &_streamInfoList[id]->streamHandle);

        loginResult = ICMS_SetStreamCallBack(_login->sSessionID, _streamInfoList[id]->deviceId, _streamInfoList[id]->streamHandle,
            fStreamCallBack, long(_streamInfoList[id]->frameCacheList.get()));
        LOG_DEBUG("ICMS_SetStreamCallBack result:%d handle:%d", loginResult, _streamInfoList[id]->streamHandle);
        return loginResult;
    }

    bool closeStreamFromDeviceId(std::string id) {
        LOG_DEBUG("ICMS_RealPlayAsync close %s", id.data());
        if(_streamInfoList.find(id) != _streamInfoList.end())
            return ICMS_StopRealPlay(_login->sSessionID, _streamInfoList[id]->deviceId, _streamInfoList[id]->streamHandle);
        return true;
    }

    // 获取设备列表
    hv::Json getDeviceList() {
        if (_login->sSessionID == nullptr) {
            LOG_DEBUG("登录状态异常");
            return {};
        }

        // 获取设备信息
        LOG_DEBUG("ICMS_GetAllDeviceItemEx start");
        ICMS_GLDATA stGlData{ "", _login->dwUserRegion, NODETYPE::ICMS_TYPE_REGION };
        ICMS_QUERY_PAGEINFO stPageInfo{ 100,0 ," " };
        // 一层信息
        ICMS_RSP_PAGEINFO m_pstRspPageInfo = {};
        ICMS_DLData m_pstDeviceData[100] = {};
        auto result = ICMS_GetAllDeviceItemEx(_login->sSessionID, stGlData, stPageInfo, &m_pstRspPageInfo, m_pstDeviceData, true);


        std::string parentId = std::to_string(m_pstDeviceData[0].dwID).append("-").
            append(m_pstDeviceData[0].sDevPath).append("-").append(m_pstDeviceData[0].sName);
        addDeviceToList(m_pstDeviceData[0]);
        LOG_DEBUG("第一层设备数量before %d", m_pstRspPageInfo.lRowNum);
        for (int i = 1; i < m_pstRspPageInfo.lRowNum; ++i) {
            recursionDevice(m_pstDeviceData[i], parentId);
        }

        LOG_DEBUG("第一层设备数量after %d", m_totalDevice.size());

        return checkStatus();
    }
private:
    // 获取设备在线状态
    hv::Json checkStatus() {
        for (auto elem : m_totalDevice) {
            ICMS_DEVICEINFO info;
            if (!ICMS_GetDeviceInfo(_login->sSessionID, elem.first.dwID, &info)) {
                if (info.bIsOnline) {
                    DeviceInfo::Ptr deviceInfo(new DeviceInfo);
                    deviceInfo->deviceId = elem.first.dwID;
                    deviceInfo->online = info.bIsOnline;
                    deviceInfo->channelType = (NODETYPE)elem.first.lNodeType;
                    memcpy(deviceInfo->sDevPath, elem.first.sDevPath, strlen(elem.first.sDevPath) + 1);
                    deviceInfo->frameCacheList = std::make_shared<StreamSourceList>();
                    addOnlineList(deviceInfo);
                    LOG_DEBUG("添加一台在线设备:%s %d-%s", elem.first.sName, elem.first.dwID, elem.first.sDevPath);
                } else {
                    LOG_DEBUG("设备不在线:%s %d-%s", elem.first.sName, elem.first.dwID, elem.first.sDevPath);
                }
            } else {
              //  LOG_DEBUG("ICMS_GetDeviceInfo error divceId:%s %d-%s", elem.first.sName, elem.first.dwID, elem.first.sDevPath);
            }
        }
        return updateLocalList();
    }

    // 递归获取所有节点信息
    void recursionDevice(const ICMS_DLData& root, const std::string& parent) {
        
        if (root.lNodeType == NODETYPE::ICMS_TYPE_DEVICE)
            return addDeviceToList(root, parent);
        if (root.lNodeType == NODETYPE::ICMS_TYPE_REGION || root.lNodeType == NODETYPE::ICMS_TYPE_PS) {
            addDeviceToList(root, parent);
            
            ICMS_GLDATA stGlData{ "", root.dwID, root.lNodeType };
            memcpy(stGlData.sDevPath, root.sDevPath, strlen(root.sDevPath) + 1);
            ICMS_QUERY_PAGEINFO stPageInfo{ 100,0 ," " };
            ICMS_RSP_PAGEINFO pstRspPageInfo = {};
            ICMS_DLData son[100] = {};
            
            auto result = ICMS_GetAllDeviceItemEx(_login->sSessionID, stGlData, stPageInfo, &pstRspPageInfo, son);
            if (result != 0) {
                LOG_DEBUG("获取设备信息失败:%d ", result);
                return;
            }
            LOG_DEBUG("获取平台下列表数量:%s %d", root.sName, pstRspPageInfo.lRowNum);
            std::string parent = std::to_string(root.dwID).append("-").append(root.sDevPath).append("-").append(root.sName);
            for (int i = 0; i < pstRspPageInfo.lRowNum; ++i) {
                if (son[i].dwID == root.dwID && !strcmp(son[i].sDevPath, root.sDevPath))
                    continue;
                if (son[i].lNodeType == NODETYPE::ICMS_TYPE_REGION ||
                    son[i].lNodeType == NODETYPE::ICMS_TYPE_PS ||
                    son[i].lNodeType == NODETYPE::ICMS_TYPE_DEVICE) {
                
                    recursionDevice(son[i], parent);
                }  
            }
        }
        
        return addDeviceToList(root, parent);
    }
    
    void addDeviceToList(ICMS_DLData data, std::string parent = "") {
        std::lock_guard<decltype(_allDevicemtx)> lk(_allDevicemtx);
        if (m_totalDevice.find(data) == m_totalDevice.end()) {
            LOG_DEBUG("添加设备:%s:%d %d-%s", data.sName, data.lNodeType, data.dwID, data.sDevPath);
            m_totalDevice[data] = parent;
        } else {
            LOG_DEBUG("设备已存在:%s:%d %d-%s", data.sName, data.lNodeType, data.dwID, data.sDevPath);
        }
    }

    void addOnlineList(const DeviceInfo::Ptr &info) {
        try {
            //std::string id = uuidEncode::create();
            std::string str = std::to_string(info->deviceId).append("-").append(info->sDevPath);
            std::lock_guard<decltype(_onlineDevicemtx)> lk(_onlineDevicemtx);
            if(_streamInfoList.find(str) == _streamInfoList.end())
                _streamInfoList[str] = info;

        }
        catch (std::exception& e) {
            LOG_DEBUG(" 添加设备异常:%s", e.what());
        }
    }

    int IncludeChinese(char* str) {
        char c;
        while (1) {
            c = *str++;
            if (c == 0) break; //如果到字符串尾则说明该字符串没有中文字符
            if (c & 0x80) //如果字符高位为1且下一字符高位也是1则有中文字符
                if (*str & 0x80) return 1;
        }
        return 0;
    }

    std::string GBKtoUTF8(const std::string& str)
    {
        std::string strout = "";
        WCHAR* strGBK;
        int len = MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, NULL, 0);
        strGBK = new WCHAR[len];
        MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, strGBK, len);

        len = WideCharToMultiByte(CP_UTF8, 0, strGBK, -1, NULL, 0, NULL, NULL);
        char* strUTF8 = new char[len];
        WideCharToMultiByte(CP_UTF8, 0, strGBK, -1, strUTF8, len, NULL, NULL);

        //memcpy(str, strUTF8, strlen(strUTF8));

        strout = strUTF8;

        delete[] strGBK;
        strGBK = NULL;
        delete[] strUTF8;
        strUTF8 = NULL;

        return strout;
    };
    // 存储列表到本地文件
    hv::Json updateLocalList() {

        auto transCode = [&](char* origin) {
            if (IncludeChinese(origin)) {
                return GBKtoUTF8(origin);
            }
            return std::string(origin);
        };

        auto to_json = [&](ICMS_DLData& data, std::string& parent, bool online = false) {
            hv::Json value;
            value["dwID"] = transCode((char*)std::to_string(data.dwID).append("-").append(data.sDevPath).append("-").append(data.sName).data());
            value["sDevPath"] = transCode(data.sDevPath);
            value["lNodeType"] = (int)data.lNodeType;
            value["dwParentId"] = transCode((char*)parent.data());
            value["sRegionName"] = transCode(data.sRegionName);
            value["sName"] = transCode(data.sName);
            value["lState"] = (int)data.lState;
            value["lChanCount"] = (int)data.lChanCount;
            value["lChanType"] = (int)data.lChanType;
            value["online"] = (int)online;
            return value;
        };

        hv::Json root;
        hv::Json deviceList;

        int j = 0;
        for (std::pair<ICMS_DLData, std::string> elem : m_totalDevice) {
            if (elem.first.lNodeType == NODETYPE::ICMS_TYPE_DEVICE ||
                elem.first.lNodeType == NODETYPE::ICMS_TYPE_REGION ||
                elem.first.lNodeType == NODETYPE::ICMS_TYPE_PS) {

                hv::Json value;
                if (elem.first.lNodeType == NODETYPE::ICMS_TYPE_DEVICE) {
                    std::string keyInfo = std::to_string(elem.first.dwID).append("-").append(elem.first.sDevPath);
                    if (_streamInfoList.find(keyInfo) != _streamInfoList.end())
                        value = to_json(elem.first, elem.second, _streamInfoList[keyInfo]->online);
                    else
                        value = to_json(elem.first, elem.second, false);
                } else {
                    if (elem.first.lNodeType == NODETYPE::ICMS_TYPE_REGION ||
                        elem.first.lNodeType == NODETYPE::ICMS_TYPE_PS)
                        value = to_json(elem.first, elem.second);
                    else
                        continue;
                }

                if (!value.is_null())
                    deviceList[j++] = value;
            }
        }

        root["deviceLists"] = deviceList;

        return root;
    };

    void timerUpdataLocalList() {
        while (true) {
            getDeviceList();
            std::this_thread::sleep_for(std::chrono::seconds(_intervalCnt));
        }
    }

    bool loadConfig() {
        std::ifstream ifs;
        ifs.open("config.json", std::ifstream::in);
        if (!ifs.is_open()) {
            LOG_DEBUG("文件路径不存在");
            return false;
        }

        hv::Json root;
        root << ifs;


        _ip = root["pullStreamIp"]; // 实际字段保存在这里, 因为知道是什么类型，所以直接用asString()，没有进行类型的判断
        _port = root["port"];
        _configPath = root["configPath"];
        _username = root["username"];
        _sercret = root["sercret"];
        _intervalCnt = root["intervalCnt"];
        ifs.close();
        return true;
    }
private:
    struct hash_name {
        size_t operator()(const ICMS_DLData& p) const {
            return std::hash<int>()(p.dwID) ^ std::hash<std::string>()(p.sDevPath);
        }
    };

    std::shared_ptr<ICMS_LOGININFO> _login;
    std::unordered_map<ICMS_DLData,std::string, hash_name>   m_totalDevice;
    ICMS_RSP_PAGEINFO m_pstRspPageInfo2 = {};
    ICMS_DLData m_pstDeviceData2[200] = {};
    std::string _ip{ "116.113.16.206" };
    unsigned long  _port = 18000;
    std::string _sercret{ "123456" };
    std::string _username{ "admin" };
    std::string _configPath;
    unsigned int _intervalCnt = 0;
    std::recursive_mutex _onlineDevicemtx;
    std::recursive_mutex _allDevicemtx;

};

void CALLBACK fLoginStatusCallBack(char* sSessionID, long lConErr, LPICMS_LOGININFO pstLogInfo, DWORD dwUser)
{
    if (lConErr != 0) {
        Pull* obj = (Pull*)dwUser;
        obj->loginPlatform();
    }
}

void CALLBACK fStreamCallBack(char* sSessionID, long lDeviceId, long lStreamHandle, LPICMS_FRAMEINFO pFrameInfo,
    BYTE* pBuffer, DWORD dwBufSize, DWORD dwUser)
{
    // 加密头(40byte)  + 前缀长度 + 编码信息(00000001)
    if (dwBufSize < (40 + 5))
        return;

    const int sercretPrefix = 40;
    // 暂不处理音频数据
    if (pFrameInfo->frame_type == 4)
        pFrameInfo->frame_type = 0x11;

    xop::AVFrame videoFrame = { 0 };
    videoFrame.type = pFrameInfo->frame_type;
    videoFrame.timestamp = xop::H264Source::GetTimestamp();
    
    int codeType = 0;
    // 处理h265 前缀长度
    int protocalSize = H264Parser::findHeader((pBuffer + sercretPrefix), dwBufSize - sercretPrefix, &codeType);
    if (protocalSize > 0 || codeType) {
        videoFrame.buffer.reset(new uint8_t[dwBufSize - sercretPrefix - protocalSize]);
        videoFrame.size = dwBufSize - sercretPrefix - protocalSize;
        memcpy(videoFrame.buffer.get(), (char*)pBuffer + sercretPrefix + protocalSize, dwBufSize - sercretPrefix - protocalSize);
        videoFrame.protocal = MediaType::H265;
    } else {
        videoFrame.buffer.reset(new uint8_t[dwBufSize - sercretPrefix]);
        videoFrame.size = dwBufSize - sercretPrefix;
        memcpy(videoFrame.buffer.get(), (char*)pBuffer + sercretPrefix, dwBufSize - sercretPrefix);
        videoFrame.protocal = MediaType::H264;
    } 

   // LOG_DEBUG("remove header videoFrame.protocal:%d lDeviceId:%d prefix size: %d", videoFrame.protocal, lDeviceId, protocalSize);
    StreamSourceList* obj = (StreamSourceList*)(dwUser);
    obj->pushFrame(videoFrame);

}

void CALLBACK fDeviceRealPlayStatusCallBack(long plstreamHandle, long lRealPlayErr, char* strDescripton) {
    LOG_DEBUG("plstreamHandle:%d lRealPlayErr:%d strDescripton:%s", plstreamHandle, lRealPlayErr, strDescripton);

}