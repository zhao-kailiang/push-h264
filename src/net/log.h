#ifndef _XOP_LOG_H
#define _XOP_LOG_H

#include <cstdio>
#include <cstring>

#define __FILENAME__ (strrchr(__FILE__, '\\') ? (strrchr(__FILE__, '\\') + 1):__FILE__)

//#ifdef _DEBUG
#define LOG(format, ...)  	\
{								\
    fprintf(stderr, "[DEBUG] [%s:%s:%d] " format "\n", \
    __FILENAME__, __FUNCTION__ , __LINE__, ##__VA_ARGS__);     \
}
//#else
//#define LOG(format, ...)  	
//#endif 


#endif
