////////////////////////////////////////////////////////////////////////////////
// Copyright (C), 2017-2020, Beijing Hanbang Technology, Inc.
////////////////////////////////////////////////////////////////////////////////
//
// File Name:   HBPlatSdk.h
// Author:      zdw  
// Version:     1.00
// Date:        
// Description: Header file of HBPlatSdk.dll and HBPlatSdk.lib.
// History:
//
////////////////////////////////////////////////////////////////////////////////
#pragma once

#ifndef ICMS_PLATSDK_H
#define ICMS_PLATSDK_H

#ifdef _LIB
#define ICMS_API
#else
#define ICMS_API extern "C" __declspec(dllexport)
#endif

#include "ICMS_CommData.h"
#include "ICMS_ErrorCode.h"
/////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
// 接口函数定义
/***************************************************初始化***********************************/
/*
功  能：SDK初始化
参  数：
      无
返回值：错误码*/
ICMS_API long  __stdcall ICMS_Init();//

/*
功  能：SDK释放
参  数：
      无
返回值：错误码*/
ICMS_API long __stdcall ICMS_Uninit();
/***************************************************登陆登出***********************************/
/*
功  能：用户登陆
参  数：
      sAddr:中心服务器ip
      dwPort：中心服务器端口
      sUserName:用户名
      sPassword：密码
      pstLogInfo:登陆返回结构体，包括会话id等
	  bIsSpec:是否定制,true为内蒙电力定制
返回值：错误码*/
ICMS_API long __stdcall ICMS_UserLogin(IN char *sAddr,IN DWORD dwPort,IN char *sUserName,IN char *sPassword,
OUT LPICMS_LOGININFO pstLogInfo,bool bIsSpec = false);

/*
功  能：用户登出
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
返回值：错误码*/
ICMS_API long __stdcall ICMS_UserLogout(IN char* sSessionID);

/*
功  能：注册平台连接自动重连回调
参  数：
    sSessionID:用户会话ID，由ICMS_UserLogin返回
	回调函数：fLoginStatusCallBack
	       sSessionID：用户会话ID，由ICMS_UserLogin返回
	       lConErr:为平台重新连接错误码，0为成功
           pstLogInfo：重连成功返回的用户会话id等
		   dwUser：用户参数
    dwUser:用户参数
返回值：错误码
*/
ICMS_API long __stdcall ICMS_LoginStatus(IN char* sSessionID,IN void (CALLBACK *fLoginStatusCallBack)(char* sSessionID,long lConErr,LPICMS_LOGININFO pstLogInfo,DWORD dwUser),IN DWORD dwUser);

/*
功  能：获取用户会话id
参  数：
    sSessionID:用户会话ID，由ICMS_UserLogin返回
返回值：错误码
*/
ICMS_API long __stdcall ICMS_GetUserSession(OUT char* sSessionId);
/*******************************设备管理************************************************/
/*
功  能：获取区域设备列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lRegionId:区域id
      stPageInfo:分页请求信息
      pstRspPageInfo:分页响应信息
      pstDeviceData:区域设备节点参数，包括多项
返回值：错误码
注释:ICMS_GetAllDeviceItem接口只能获取本级区域下列表，ICMS_GetAllDeviceItemEx也可获取级联平台下列表
*/
ICMS_API long __stdcall ICMS_GetAllDeviceItem(IN char *sSessionID,IN long lRegionId,IN  ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_DLData pstDeviceData);
/*
功  能：获取区域设备列表扩展
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stDlData:区域级联信息
      stPageInfo:分页请求信息
      pstRspPageInfo:分页响应信息
      pstDeviceData:区域设备节点参数，包括多项
	  bIsReGet:是否强制重新获取设备列表，默认FALSE不强制
返回值：错误码
注释:ICMS_GetAllDeviceItem接口只能获取本级区域下列表，ICMS_GetAllDeviceItemEx也可获取级联平台下列表
*/
ICMS_API long __stdcall ICMS_GetAllDeviceItemEx(IN char *sSessionID,IN ICMS_GLDATA stGlData,IN  ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_DLData pstDeviceData,IN BOOL bIsReGet = FALSE);
/*
功  能：添加设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stdeviceInfo:设备参数
      plDeviceId:返回设备id
返回值：错误码*/
ICMS_API long __stdcall ICMS_AddDevice(IN char* sSessionID,IN ICMS_DEVICEINFO stdeviceInfo,OUT long *plDeviceId);
/*
功  能：添加设备扩展
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     stdeviceInfo:设备参数
     pstGldata:返回设备节点信息
返回值：错误码*/
ICMS_API long __stdcall ICMS_AddDeviceEx(IN char* sSessionID, IN ICMS_DEVICEINFO stdeviceInfo, OUT LPICMS_GLDATA pstGldata);

/*
功  能：删除设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lDeviceId:设备id
返回值：错误码*/
ICMS_API long __stdcall ICMS_DeleteDevice(IN char* sSessionID,IN  long lDeviceId);

/*
功  能：删除设备扩展
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      stGldata:设备节点信息
返回值：错误码*/
ICMS_API long __stdcall ICMS_DeleteDeviceEx(IN char* sSessionID, IN ICMS_GLDATA stGldata);

/*
功  能：修改设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stdeviceInfo:设备参数
      lDeviceId:设备id
返回值：错误码*/
ICMS_API long __stdcall ICMS_ModifyDevice(IN char* sSessionID,IN  ICMS_DEVICEINFO stdeviceInfo,IN long lDeviceId);

/*
功  能：修改设备扩展
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      stdeviceInfo:设备参数
      stGldata:设备节点信息
返回值：错误码*/
ICMS_API long __stdcall ICMS_ModifyDeviceEx(IN char* sSessionID, IN  ICMS_DEVICEINFO stdeviceInfo, IN ICMS_GLDATA stGldata);
/*
功  能：搜索设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  回调函数：fDeviceSearchCallBack
	            pstSearchDeviceInfo：返回搜索设备参数，每次返回一个
	            dwUser：用户参数
	  dwUser:用户参数
返回值：错误码*/
ICMS_API long __stdcall ICMS_SearchDevice(IN char *sSessionID,IN void (CALLBACK *fDeviceSearchCallBack)(char* sSessionID,LPICMS_SEARCH_DEVICEINFO pstSearchDeviceInfo, DWORD dwUser),IN  DWORD dwUser);

/*
功  能：停止搜索设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
返回值：错误码*/
ICMS_API long __stdcall ICMS_StopSearchDevice(IN char *sSessionID);

/*
功  能：注册设备状态回调
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  回调函数：fDeviceStatusCallBack
	            sSessionID：会话id
				lDeviceId：设备id
				lConErr：错误号
				stDldata：设备参数
				sDeviceMac:设备上线后获取到mac地址
                lBf：是否布防
	            dwUser：用户参数
	  dwUser:用户参数
返回值：错误码*/
ICMS_API long __stdcall ICMS_DeviceStatus(IN char* sSessionID,IN void (CALLBACK *fDeviceStatusCallBack)(char* sSessionID,long lDeviceId,long lConErr,LPICMS_DLData stDldata,char* sDeviceMac,long lBf, DWORD dwUser),IN  DWORD dwUser);

/*
功  能：获取设备信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
      pstdeviceInfo:设备信息
返回值：错误码*/
ICMS_API long __stdcall ICMS_GetDeviceInfo(IN char* sSessionID,IN long lDeviceId ,OUT LPICMS_DEVICEINFO pstdeviceInfo);

/*
功  能：获取设备信息扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	pstdeviceInfo:设备信息
返回值：错误码*/
ICMS_API long __stdcall ICMS_GetDeviceInfoEx(IN char* sSessionID, IN ICMS_GLDATA stGldata ,OUT LPICMS_DEVICEINFO pstdeviceInfo);
/*
功  能：注册配置变更通知回调
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  回调函数：fChangeConfigCallBack
	            sSessionID：会话id
				lType：节点变更类型-增加，修改，删除
				pstDlData：节点信息
				lConErr：错误号
				lCount：返回节点个数
	            dwUser：用户参数
	  dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RegisterChangeConfig(IN char* sSessionID,IN void(CALLBACK *fChangeConfigCallBack)(char* sSessionID,long lType,long lError,LPICMS_DLData pstDlData,long lCount,DWORD dwUser),IN DWORD dwUser);
/*****************实时预览**************************/
/*
功  能：打开实时流播放
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  stClientInfo:实时流信息
	  plstreamHandle:返回视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RealPlay(IN char* sSessionID,IN long lDeviceId,IN ICMS_CLIENTINFO stClientInfo,
OUT long *plstreamHandle);

/*
功  能：打开实时流播放扩展
参  数：
        sSessionID:用户会话ID，由ICMS_UserLogin返回
		stGldata:设备节点信息
		stClientInfo:实时流信息
		plstreamHandle:返回视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RealPlayEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN ICMS_CLIENTINFO stClientInfo, OUT long *plstreamHandle);
/*
功  能：异步打开实时流播放
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	stClientInfo:实时流信息
	fDeviceRealPlayStatusCallBack：回调函数
	        plstreamHandle：视频流的id值
			lRealPlayErr：错误号
			strDescripton：错误描述
//			sSessionID：会话id
//			lDeviceId：设备id
//			lChannelId：通道id
//			lRealPlayErr：错误号
//			dwUser：用户参数
	dwUser:用户参数
	plstreamHandle:返回视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RealPlayAsync(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN ICMS_CLIENTINFO stClientInfo,
	IN void (CALLBACK *fDeviceRealPlayStatusCallBack)(long plstreamHandle, long lRealPlayErr,char* strDescripton), IN DWORD dwUser, OUT long *plstreamHandle);

/*
功  能：局部放大或缩小显示
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  fx:放大点在窗口的宽比例
	  fy:放大点在窗口的高比例
	  hWndBk:小图句柄
	  bIsUp:true为放大，false为缩小
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_WheelUpOrDown(IN char* sSessionID, IN long lDeviceId, IN long lStreamHandle, IN float fx, IN float fy, IN HWND hWndBk, IN bool bIsUp = true);

/*
功  能：局部放大或缩小显示扩展
参  数：
	 sSessionID:用户会话ID，由ICMS_UserLogin返回
	 stGldata:设备节点信息
	 lStreamHandle:视频流的id值
	 fx:放大点在窗口的宽比例
	 fy:放大点在窗口的高比例
	 hWndBk:小图句柄
	 bIsUp:true为放大，false为缩小
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_WheelUpOrDownEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN float fx, IN float fy, IN HWND hWndBk, IN bool bIsUp = true);

/*
功  能：移动局部小图显示
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lDeviceId:设备id
      lStreamHandle:视频流的id值
      ftop:矩形顶在小窗口的比例
      fdown:矩形底在小窗口的比例
	  fleft:矩形左在小窗口的比例
	  fright:矩形右在小窗口的比例
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_MoveThumbnails(IN char* sSessionID, IN long lDeviceId, IN long lStreamHandle, IN float ftop, IN float fbottom, IN float fleft, IN float fright);

/*
功  能：移动局部小图显示扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	ftop:矩形顶在小窗口的比例
	fdown:矩形底在小窗口的比例
	fleft:矩形左在小窗口的比例
	fright:矩形右在小窗口的比例
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_MoveThumbnailsEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN float ftop, IN float fbottom, IN float fleft, IN float fright);

/*
功  能：停止获取实时流
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopRealPlay(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle);

/*
功  能：停止获取实时流扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopRealPlayEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle);

/*
功  能：获取声音播放状态
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     stGldata:设备节点信息
	 lStatus:返回状态，0-为关闭声音，1,-为开启声音
返回值：返回错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetSoundStatus(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long  lStreamHandle,OUT long* plStatus);
/*
功  能：播放声音
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenSound(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle);

/*
功  能：播放声音扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenSoundEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle);
/*
功  能：关闭声音
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopSound(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle);

/*
功  能：关闭声音扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopSoundEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle);

// 注册获取解码前实时视频流
ICMS_API long __stdcall ICMS_SetStreamCallBack(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,
	IN void (CALLBACK *fStreamCallBack) (char* sSessionID ,long lDeviceId,long lStreamHandle,LPICMS_FRAMEINFO pFrameInfo,
	BYTE *pBuffer, DWORD dwBufSize, DWORD dwUser),IN DWORD dwUser);

// 注册获取解码后实时视频流
ICMS_API long __stdcall ICMS_SetStreamDecodeCallBack(IN char* sSessionID , 
	IN void (CALLBACK *fStreamDecodeCallBack) (char* sSessionID ,long lDeviceId,long lStreamHandle
	, BYTE *pBuffer, DWORD dwBufSize, DWORD dwUser),IN DWORD dwUser);

//设置播放模式
ICMS_API long __stdcall ICMS_SetRTStreamMode(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,IN long lPlayMode);

ICMS_API long __stdcall ICMS_SetRTStreamModeEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN long lPlayMode);
/*
功  能：开始本地录像
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  sFilePathName:带文件名的文件路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StartRecord(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,IN char* sFilePathName);

/*
功  能：开始本地录像扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	sFilePathName:带文件名的文件路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StartRecordEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN char* sFilePathName);
/*
功  能：停止本地录像
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopRecord(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle);

/*
功  能：停止本地录像扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopRecordEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle);
/*
功  能：抓拍图片
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  sFilePathName:带文件名的文件路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CapturePic(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,IN char* sFilePathName);

/*
功  能：抓拍图片扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	sFilePathName:带文件名的文件路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CapturePicEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN char* sFilePathName);
/*
功  能：获取视频压缩参数范围
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  pstMainVideoCompressParam:主码流范围
	  pstChildVideoCompressParam:子码流范围
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoCompressRange(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,
	OUT LPICMS_VIDEO_COMPRESS_RANGE pstMainVideoCompressParam,OUT LPICMS_VIDEO_COMPRESS_RANGE pstChildVideoCompressParam);

/*
功  能：获取视频压缩参数范围扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	pstMainVideoCompressParam:主码流范围
	pstChildVideoCompressParam:子码流范围
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoCompressRangeEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle,
	OUT LPICMS_VIDEO_COMPRESS_RANGE pstMainVideoCompressParam, OUT LPICMS_VIDEO_COMPRESS_RANGE pstChildVideoCompressParam);

/*
功  能：获取视频压缩参数
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  pstMainVideoCompressParam:主码流参数
	  pstChildVideoCompressParam:子码流参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoCompressParam(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,
	OUT LPICMS_VIDEO_COMPRESS_PARAM pstMainVideoCompressParam,OUT LPICMS_VIDEO_COMPRESS_PARAM pstChildVideoCompressParam);
/*
功  能：获取视频压缩参数扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	pstMainVideoCompressParam:主码流参数
	pstChildVideoCompressParam:子码流参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoCompressParamEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, 
	OUT LPICMS_VIDEO_COMPRESS_PARAM pstMainVideoCompressParam, OUT LPICMS_VIDEO_COMPRESS_PARAM pstChildVideoCompressParam);

/*
功  能：设置视频压缩参数
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  stMainVideoCompressParam:主码流参数
	  stChildVideoCompressParam:子码流参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetVideoCompressParam(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,
	IN ICMS_VIDEO_COMPRESS_PARAM stMainVideoCompressParam,IN ICMS_VIDEO_COMPRESS_PARAM stChildVideoCompressParam);
/*
功  能：设置视频压缩参数扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	stMainVideoCompressParam:主码流参数
	stChildVideoCompressParam:子码流参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetVideoCompressParamEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle,
	IN ICMS_VIDEO_COMPRESS_PARAM stMainVideoCompressParam, IN ICMS_VIDEO_COMPRESS_PARAM stChildVideoCompressParam);

/*
功  能：获取视频图像参数范围
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  pstMinVideoPicParam:最小值
	  pstMaxVideoPicParam:最大值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoPicParamRange(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,
	OUT LPICMS_VIDEO_PIC_PARAM pstMinVideoPicParam,OUT LPICMS_VIDEO_PIC_PARAM pstMaxVideoPicParam);
/*
功  能：获取视频图像参数范围扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	pstMinVideoPicParam:最小值
	pstMaxVideoPicParam:最大值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoPicParamRangeEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, 
	OUT LPICMS_VIDEO_PIC_PARAM pstMinVideoPicParam, OUT LPICMS_VIDEO_PIC_PARAM pstMaxVideoPicParam);

/*
功  能：获取视频图像参数
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  pstVideoPicParam:图像参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoPicParam(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,
	OUT LPICMS_VIDEO_PIC_PARAM pstVideoPicParam);

/*
功  能：获取视频图像参数扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	pstVideoPicParam:图像参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoPicParamEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle,
	OUT LPICMS_VIDEO_PIC_PARAM pstVideoPicParam);

/*
功  能：设置视频参数
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  stVideoPicParam:图像参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetVideoPicParam(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,
	IN ICMS_VIDEO_PIC_PARAM stVideoPicParam);
/*
功  能：设置视频参数扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	stVideoPicParam:图像参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetVideoPicParamEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle,
	IN ICMS_VIDEO_PIC_PARAM stVideoPicParam);

/*
功  能：获取osd参数
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  pstOsdParam:OSD参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetOsdParam(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,OUT LPICMS_OSD_PARAM pstOsdParam);

/*
功  能：获取osd参数扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	lDeviceId:设备id
	lStreamHandle:视频流的id值
	pstOsdParam:OSD参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetOsdParamEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, OUT LPICMS_OSD_PARAM pstOsdParam);

/*
功  能：设置osd参数
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  stOsdParam:OSD参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetOsdParam(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,IN ICMS_OSD_PARAM stOsdParam);
/*
功  能：设置osd参数扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	stOsdParam:OSD参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetOsdParamEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN ICMS_OSD_PARAM stOsdParam);

/*
功  能：获取区域屏蔽
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  pstVideoBlock:区域屏蔽参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoBlock(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,OUT LPICMS_VIDEO_BLOCK pstVideoBlock);
/*
功  能：获取区域屏蔽扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	pstVideoBlock:区域屏蔽参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetVideoBlockEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, OUT LPICMS_VIDEO_BLOCK pstVideoBlock);

/*
功  能：设置区域屏蔽
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  stVideoBlock:区域屏蔽参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetVideoBlock(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,IN ICMS_VIDEO_BLOCK stVideoBlock);
/*
功  能：设置区域屏蔽扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	stVideoBlock:区域屏蔽参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetVideoBlockEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN ICMS_VIDEO_BLOCK stVideoBlock);

/*
功  能：区域屏蔽复制到通道
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  stVideoBlock:区域屏蔽参数
	  stArray：通道数组
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CopyVideoBlock(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,IN ICMS_VIDEO_BLOCK stVideoBlock,IN ICMS_ARRAY stArray);
/*
功  能：区域屏蔽复制到通道扩展
参  数：
		sSessionID:用户会话ID，由ICMS_UserLogin返回
		stGldata:设备节点信息
		lStreamHandle:视频流的id值
		stVideoBlock:区域屏蔽参数
		stArray：通道数组
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CopyVideoBlockEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN ICMS_VIDEO_BLOCK stVideoBlock, IN ICMS_ARRAY stArray);

/*
功  能：获取定时录像
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  pstRecordTime:录像参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetRecTime(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,OUT LPICMS_RECORD_TIME pstRecordTime);

/*
功  能：获取定时录像扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	pstRecordTime:录像参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetRecTimeEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, OUT LPICMS_RECORD_TIME pstRecordTime);

/*
功  能：设置定时录像
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  stRecordTime:录像参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetRecTime(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,IN ICMS_RECORD_TIME stRecordTime);

/*
功  能：设置定时录像扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	stRecordTime:录像参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetRecTimeEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN ICMS_RECORD_TIME stRecordTime);

/*
功  能：定时录像复制到通道
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  stRecordTime:录像参数
	  stArray:通道数组
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CopyRecTime(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,IN ICMS_RECORD_TIME stRecordTime,IN ICMS_ARRAY stArray);

/*
功  能：定时录像复制到通道扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	stRecordTime:录像参数
	stArray:通道数组
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CopyRecTimeEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN ICMS_RECORD_TIME stRecordTime, IN ICMS_ARRAY stArray);
/*****************历史回放**************************/
/*
功  能：获取存储服务器设备录像状态
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      stGldata:设备节点信息
      lChannId:通道id
      lStoreStrageyId:存储策略id
	  pStatus:返回设备录像状态，0-未录像，1-正在录像，2-录像错误
返回值：错误码
*/
ICMS_API long __stdcall ICMS_GetDevStoreServerStatus(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lChannel, IN long lStoreStrategyId, OUT long* pStatus);
/*
功  能：手动存储服务器录像
参  数：
        sSessionID:用户会话ID，由ICMS_UserLogin返回
        stGldata:设备节点信息
		lChannId:通道id
		lStoreStrageyId:存储策略id
		fManualRecordCallBack:回调函数
			sSessionID：会话id
			lDeviceId：设备id
			lChannelId：通道id
			lErr：错误号,0为成功
			dwUser：用户参数
		dwUser:用户参数	    
返回值：错误码
*/
ICMS_API long __stdcall ICMS_StartManualRecordEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lChannId, IN long lStoreStrageyId,
	IN void (CALLBACK *fManualRecordCallBack)(char* sSessionID, long lDeviceId, long lChannlId, long lErr, DWORD dwUser), IN DWORD dwUser);

/*
功  能：停止手动存储服务器录像
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      stGldata:设备节点信息
	  lChannId:通道id
	  lStoreStrageyId:存储策略id
	  fManualRecordCallBack:回调函数
		  sSessionID：会话id
		  lDeviceId：设备id
		  lChannelId：通道id
		  lErr：错误号,0为成功
		  dwUser：用户参数
	  dwUser:用户参数
返回值：错误码
*/
ICMS_API long __stdcall ICMS_StopManualRecordEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lChannId, IN long lStoreStrageyId,
	IN void (CALLBACK *fManualRecordCallBack)(char* sSessionID, long lDeviceId, long lChannlId, long lErr, DWORD dwUser), IN DWORD dwUser);

/*
功  能：查询历史视频
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  fHisQueryCallBack:回调历史视频列表
	       pstRspPageInfo：当获取设备所有通道列表时，当某个通道出错时lRowNum为返回错误码，lTotalRowNum为通道号; 否则按正常返回
	       pstHisInfo：查询的一个通道的所有记录信息，当一个设备有n个通道时，一次回调所有通道的列表      
返回值：错误码*/
ICMS_API long __stdcall ICMS_GetHisItem(IN char* sSessionID,IN long lDeviceId,IN ICMS_HIS_VIDEO stHisVideo,IN  ICMS_QUERY_PAGEINFO stPageInfo,
		IN void (CALLBACK *fHisQueryCallBack) (char* sSessionID ,long lDeviceId,LPICMS_RSP_PAGEINFO pstRspPageInfo,LPICMS_QUERY_HIS pstHisInfo, 
		DWORD dwUser),IN DWORD dwUser);

/*
功  能：查询历史视频扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	fHisQueryCallBack:回调历史视频列表
	pstRspPageInfo：当获取设备所有通道列表时，当某个通道出错时lRowNum为返回错误码，lTotalRowNum为通道号; 否则按正常返回
	pstHisInfo：查询的一个通道的所有记录信息，当一个设备有n个通道时，一次回调所有通道的列表
返回值：错误码*/
ICMS_API long __stdcall ICMS_GetHisItemEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN ICMS_HIS_VIDEO stHisVideo, IN  ICMS_QUERY_PAGEINFO stPageInfo,
	IN void (CALLBACK *fHisQueryCallBack) (char* sSessionID, long lDeviceId, LPICMS_RSP_PAGEINFO pstRspPageInfo, LPICMS_QUERY_HIS pstHisInfo,
	DWORD dwUser), IN DWORD dwUser);
/*
功  能：下载历史视频
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  sFilePath:文件存储路径
	  pstDownHisInfo:下载文件结构体--可下载多个文件
	  lCount:实际下载文件个数
	  bIsAvi:是否转为avi格式
      fDownLoadCallBack:回调
	         sSessionID：用户会话ID，由ICMS_UserLogin返回
			 lFileNum:文件编号，以0开始
			 lStatus:历史视频下载状态
			 lParam:参数值，错误或者进度
			 dwUser:用户参数
	 dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DownLoadHis(IN char* sSessionID,IN long lDeviceId,IN char* sFilePath,IN LPICMS_DOWNLOAD_HIS pstDownHisInfo,IN long lCount,IN bool bIsAvi
										 ,IN void (CALLBACK *fDownLoadCallBack)(char* sSessionID,long lFileNum,long lStatus,long lParam, DWORD dwUser),IN  DWORD dwUser);
/*
功  能：下载历史视频扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	sFilePath:文件存储路径
	pstDownHisInfo:下载文件结构体--可下载多个文件
	lCount:实际下载文件个数
	bIsAvi:是否转为avi格式
	fDownLoadCallBack:回调
	sSessionID：用户会话ID，由ICMS_UserLogin返回
	lFileNum:文件编号，以0开始
	lStatus:历史视频下载状态
	lParam:参数值，错误或者进度
	dwUser:用户参数
	dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DownLoadHisEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN char* sFilePath, IN LPICMS_DOWNLOAD_HIS pstDownHisInfo, IN long lCount, IN bool bIsAvi
	, IN void (CALLBACK *fDownLoadCallBack)(char* sSessionID, long lFileNum, long lStatus, long lParam, DWORD dwUser), IN  DWORD dwUser);

/*
功  能：停止下载历史视频
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopDownLoadHis(IN char* sSessionID,IN long lDeviceId);
/*
功  能：停止下载历史视频扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopDownLoadHisEx(IN char* sSessionID, IN ICMS_GLDATA stGldata);

/*
功  能：断点续传
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  iSelNum:第几个文件，以0开始
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ContinueDownLoad(IN char* sSessionID,IN long lDeviceId,IN int iSelNum);

/*
功  能：断点续传扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	iSelNum:第几个文件，以0开始
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ContinueDownLoadEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN int iSelNum);
/*
功  能：移除队列
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  iSelNum:第几个文件，以0开始
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RemoveDownLoad(IN char* sSessionID,IN long lDeviceId,IN int iSelNum);
/*
功  能：移除队列扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	iSelNum:第几个文件，以0开始
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RemoveDownLoadEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN int iSelNum);
//本地打开
//ICMS_API long __stdcall ICMS_ContinueDownLoad(IN char* sSessionID,IN long lDeviceId,IN long lSelNum);

/*
功  能：重新下载
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  iSelNum:第几个文件，以0开始
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RetryDownLoad(IN char* sSessionID,IN long lDeviceId,IN int iSelNum);

/*
功  能：重新下载扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	iSelNum:第几个文件，以0开始
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RetryDownLoadEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN int iSelNum);
/*
功  能：打开历史视频流
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  stHisInfo:历史流播放信息
	  plStreamHandle:返回视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenHisPlay(IN char* sSessionID,IN long lDeviceId,IN ICMS_HIS_CLIENTINFO stHisInfo,OUT long* plStreamHandle);
/*
功  能：打开历史视频流扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	stHisInfo:历史流播放信息
	plStreamHandle:返回视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenHisPlayEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN ICMS_HIS_CLIENTINFO stHisInfo, OUT long* plStreamHandle);

//注册获取历史视频流
ICMS_API long __stdcall ICMS_SetStreamHisCallBack(IN char* sSessionID ,IN void (CALLBACK *fStreamHisCallBack) (char* sSessionID ,long lDeviceId,LONG lStreamHandle,long lChannel,
	LPICMS_FRAMEINFO pstFrameInfo, BYTE *pBuffer, DWORD dwBufSize, DWORD dwUser),IN DWORD dwUser);

/*
功  能：停止获取历史视频流
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopHisPlay(IN char* sSessionID,IN long lDeviceId,IN long  lStreamHandle);

/*
功  能：停止获取历史视频流扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StopHisPlayEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long  lStreamHandle);
/*
功  能：暂停播放历史视频流
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PauseHisPlay(IN char* sSessionID,IN long lDeviceId,IN long  lStreamHandle);

/*
功  能：暂停播放历史视频流扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PauseHisPlayEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long  lStreamHandle);
/*
功  能：恢复播放历史视频
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ReOpenHisPlay(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle);
/*
功  能：恢复播放历史视频扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ReOpenHisPlayEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle);
/*
功  能：设置播放速度
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  lSpeed:播放速度大小
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetHisPlaySpeed(IN char* sSessionID,IN long lDeviceId,IN long  lStreamHandle,IN long lSpeed);
/*
功  能：设置播放速度扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	lSpeed:播放速度大小
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetHisPlaySpeedEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long  lStreamHandle, IN long lSpeed);

/*
功  能：获取播放速度
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  plSpeed:返回播放速度大小
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetHisPlaySpeed (IN char* sSessionID,IN long lDeviceId,IN long  lStreamHandle,OUT long * plSpeed);
/*
功  能：获取播放速度扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	plSpeed:返回播放速度大小
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetHisPlaySpeedEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long  lStreamHandle, OUT long * plSpeed);

/*
功  能：获取播放位置
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  pSysTime:返回当前播放时间
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetHisPlayPos (IN char* sSessionID,IN long lDeviceId,IN long  lStreamHandle,OUT LPICMS_TIME pSysTime);
/*
功  能：获取播放位置扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	pSysTime:返回当前播放时间
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetHisPlayPosEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long  lStreamHandle, OUT LPICMS_TIME pSysTime);

/*
功  能：设置播放位置
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流的id值
	  sysTime:设置播放时间
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetHisPlayPos (IN char* sSessionID,IN long lDeviceId,IN long  lStreamHandle,IN ICMS_TIME sysTime);
/*
功  能：设置播放位置扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流的id值
	sysTime:设置播放时间
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_SetHisPlayPosEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long  lStreamHandle, IN ICMS_TIME sysTime);

/**********************区域、用户管理**********************************************/
/*
功  能：添加区域
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stRegionInfo:区域信息
	  plRegionId:返回区域id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddRegion(IN char* sSessionID,IN ICMS_REGIONINFO stRegionInfo,OUT long *plRegionId);
/*
功  能：添加区域扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stRegionInfo:区域信息
	pstGldata:返回区域节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddRegionEx(IN char* sSessionID, IN ICMS_REGIONINFO stRegionInfo, OUT LPICMS_GLDATA pstGldata);

/*
功  能：添加区域(内蒙电力使用)
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     pstRegionInfo:区域扩展信息，返回父区域id
     pstGldata:返回区域节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddRegionSpec(IN char* sSessionID, IN OUT LPICMS_REGIONINFO_EX pstRegionInfo, OUT LPICMS_GLDATA pstGldata);

/*
功  能：删除区域
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lRegionId:区域id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DeleteRegion(IN char* sSessionID,IN long lRegionId);
/*
功  能：删除区域扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:区域节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DeleteRegionEx(IN char* sSessionID, IN ICMS_GLDATA stGldata);

/*
功  能：删除区域（内蒙电力使用）
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
	 sBaseKeyId:区域keyid
     stGldata:区域节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DeleteRegionSpec(IN char* sSessionID,IN char* sBaseKeyId, OUT LPICMS_GLDATA pstGldata);
/*
功  能：修改区域
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lRegionId:区域id
	  stRegionInfo：区域信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyRegion(IN char* sSessionID,IN long lRegionId,IN  ICMS_REGIONINFO stRegionInfo);

/*
功  能：修改区域扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:区域节点信息
	stRegionInfo：区域信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyRegionEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN  ICMS_REGIONINFO stRegionInfo);

/*
功  能：修改区域（内蒙电力使用）
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
	 sBaseKeyId:区域keyid
     stGldata:区域节点信息
     stRegionInfo：区域信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyRegionSpec(IN char* sSessionID, IN  ICMS_REGIONINFO_EX stRegionInfo, OUT LPICMS_GLDATA pstGldata);

/*
功  能：获取区域信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lRegionId:区域id
	  pstRegionInfo:返回区域信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetRegionInfo(IN char* sSessionID,IN long lRegionId,OUT LPICMS_REGIONINFO pstRegionInfo);
/*
功  能：获取区域信息扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:区域节点信息
	pstRegionInfo:返回区域信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetRegionInfoEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, OUT LPICMS_REGIONINFO pstRegionInfo);

/*
功  能：获取区域信息(内蒙电力使用)
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     stGldata:区域节点信息
     pstRegionInfo:返回区域扩展信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetRegionInfoSpec(IN char* sSessionID, IN ICMS_GLDATA stGldata, OUT LPICMS_REGIONINFO_EX pstRegionInfo);

/*
功  能：获取用户组列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  sPsPath:级联路径
	  stPageInfo：分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstUserGroupInfo:返回用户组信息-包括多项最大200项
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetUserGroupList(IN char* sSessionID,IN char* sPsPath,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_USERGROUPINFO pstUserGroupInfo);

/*
功  能：获取用户组信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId：用户组id
	  sPsPath:级联路径
	  pstUserGroupInfo:返回用户组信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetUserGroupInfo(IN char* sSessionID,IN long lUserGroupId,IN char* sPsPath,OUT LPICMS_USERGROUPINFO pstUserGroupInfo);

/*
功  能：添加用户组
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  plUserGroupId：返回用户组id
	  sPsPath:级联路径
	  pstUserGroupInfo:用户组信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddUserGroupInfo(IN char* sSessionID,IN char* sPsPath,IN LPICMS_USERGROUPINFO pstUserGroupInfo,OUT long *plUserGroupId);

/*
功  能：修改用户组
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId：用户组id
	  sPsPath:级联路径
	  pstUserGroupInfo:用户组信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyUserGroupInfo(IN char* sSessionID,IN long lUserGroupId,IN char* sPsPath,IN LPICMS_USERGROUPINFO pstUserGroupInfo);

/*
功  能：删除用户组
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId：用户组id
	  sPsPath:级联路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelUserGroupInfo(IN char* sSessionID,IN long lUserGroupId,IN char* sPsPath);

/*
功  能：获取某用户组的用户列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId：用户组id
	  sPsPath:级联路径
	  stPageInfo:分页请求信息
      pstRspPageInfo:分页响应信息
      pstUserInfo：返回用户列表-包括多项
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetUserList(IN char* sSessionID,IN long lUserGroupId,IN char* sPsPath,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_USERINFO pstUserInfo);

/*
功  能：获取用户信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserId:用户id
	  sPsPath:级联路径
      pstUserInfo：返回用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetUserInfo(IN char* sSessionID,IN long lUserGroupId,IN long lUserId,IN char* sPsPath,OUT LPICMS_USERINFO pstUserInfo);

/*
功  能：添加用户
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  sPsPath:级联路径
	  plUserId:返回用户id
      stUserInfo：用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddUserInfo(IN char* sSessionID,IN long lUserGroupId,IN char* sPsPath,IN LPICMS_USERINFO pstUserInfo,OUT long* plUserId);

/*
功  能：删除用户
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  lUserId:用户id
	  sPsPath:级联路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DeleUserInfo(IN char* sSessionID,IN long lUserGroupId,IN  long lUserId,IN char* sPsPath);

/*
功  能：修改用户
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  lUserId:用户id
	  stUserInfo：用户信息
	  sPsPath:级联路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyUserInfo(IN char* sSessionID,IN long lUserGroupId, IN LPICMS_USERINFO pstUserInfo,IN long lUserId,IN char* sPsPath);

/*
功  能：获取用户组通道信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  sPsPath:级联路径
	  stPageInfo:分页请求信息
	  pstRspPageInfo：分页响应信息
	  pstChann:返回多项通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetUserGroupChann(IN char* sSessionID,IN long lUserGroupId,IN bool IsIncludeLow,IN char* sPsPath,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_DLData pstChann);

/*
功  能：获取用户通道信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  lUserId:用户id
	  stPageInfo:分页请求信息
	  pstRspPageInfo：分页响应信息
	  pstChann:返回多项通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetUserChann(IN char* sSessionID,IN long lUserGroupId,IN long lUserId,IN bool IsIncludeLow,IN char* sPsPath,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_DLData pstChann);

/*
功  能：批量添加用户组通道信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  sPsPath:级联路径
	  pstChann:多项通道信息，只需设备和通道id
	  lReCount:通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddUserGroupChann(IN char* sSessionID,IN long lUserGroupId,IN char* sPsPath,IN LPICMS_DLData pstChann,IN long lReCount);

/*
功  能：批量添加用户通道信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  lUserId：用户id
	  sPsPath:级联路径
	  pstChann:多项通道信息，只需设备和通道id
	  lReCount:通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddUserChann(IN char* sSessionID,IN long lUserGroupId,IN long lUserId,IN char* sPsPath,IN LPICMS_DLData pstChann,IN long lReCount);

/*
功  能：批量删除用户组通道信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  sPsPath:级联路径
	  pstChann:多项通道信息，只需设备和通道id
	  lReCount:通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelUserGroupChann(IN char* sSessionID,IN long lUserGroupId,IN char* sPsPath,IN LPICMS_CHANN pstChann,IN long lReCount);

/*
功  能：批量删除用户通道信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  lUserId：用户id
	  sPsPath:级联路径
	  pstChann:多项通道信息，只需设备和通道id
      lReCount:通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelUserChann(IN char* sSessionID,IN long lUserGroupId,IN long lUserId,IN char* sPsPath,IN LPICMS_CHANN pstChann,IN long lReCount);

/*
功  能：修改当前登陆用户密码
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  sOldPwd:旧密码
	  sNewPwd：新密码
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyUserPwd(IN char* sSessionID,IN char *sOldPwd,IN char *sNewPwd);

/*
功  能：重置密码
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lUserGroupId:用户组id
	  lUserId：用户id
	  sOldPwd:旧密码
	  sNewPwd：新密码
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ResetUserPwd(IN char* sSessionID,IN long lUserGroupId,IN long lUserId,char* sPsPath);
/****************************服务器管理*************************************/
/*
功  能：获取服务器列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstPlatService：服务列表-包括多项
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetServiceItem(IN char *sSessionID,IN  ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_SERVICE  pstPlatService);

/*
功  能：增加服务器
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
返回值：错误码
注  释:*/
//ICMS_API long __stdcall ICMS_AddService(char *sSessionID,BOOL (CALLBACK *fServiceAddCallBack)( char* sSessionID, LPICMS_SERVICE  pstPlatService, DWORD dwUser), DWORD dwUser);

/*
功  能：修改服务器
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPlatService：服务器信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyService(IN char *sSessionID,IN ICMS_SERVICE  stPlatService);

/*
功  能：获取服务器信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  bServiceType：服务器类型
	  pstPlatService：返回服务器信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetServiceInfo(IN char *sSessionID,IN long lServerId,IN BYTE bServiceType,OUT LPICMS_SERVICE  pstPlatService);

/*
功  能：删除服务器
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  bServiceType：服务器类型
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelService(IN char *sSessionID,IN long lServerId,IN BYTE bServiceType);

/****************流媒体管理**************************/
/*
功  能：获取流媒体管理设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pChanCount:返回通道个数
	  pOnlineCount:返回在线设备个数
	  pstDeviceData:返回管理设备列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetMediaMngItem(IN char *sSessionID, IN long lServerId,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT long *pChanCount,OUT long* pOnlineCount,OUT LPICMS_DLData pstDeviceData);

//设置流媒体管理设备
//ICMS_API long __stdcall ICMS_SetMediaMngItem(char *sSessionID, long lServerId,long lRowNum, LPICMS_DLData pstDeviceData);

/*
功  能：增加管理通道项
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  stDlData：节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddMediaMngItem(IN char *sSessionID, IN long lServerId,IN  ICMS_DLData stDlData);

/*
功  能：删除管理通道项
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  lItemId:项id
	  lType:项类型
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelMediaMngItem(IN char *sSessionID,IN  long lServerId,IN  long lItemId,IN long lType);

/*****************存储磁盘管理**************************/
/*
功  能：获取存储服务器所有磁盘列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lStoreServiceId：存储服务器id
	  plReCount:返回磁盘个数，最大128个磁盘
	  pstDiskList:磁盘列表,不带id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetStoreAllDiskList(IN char *sSessionID,IN long lStoreServiceId,OUT long* plReCount,OUT LPICMS_DISK pstDiskList);

/*
功  能：获取存储服务器已使用磁盘
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lStoreServiceId：存储服务器id
	  plReCount:返回磁盘个数，最大128个磁盘
	  pstDiskList:磁盘列表，磁盘带id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetStoreUseDiskList(IN char *sSessionID,IN long lStoreServiceId,OUT long* plReCount,OUT LPICMS_DISK pstDiskList);

/*
功  能：判断磁盘是否正在被策略使用
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	lStoreServiceId：存储服务器id
	lDiskId:磁盘id
	plIsUse:返回是否策略在使用，0为未使用，1为使用
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_StoreDiskIsUse(IN char* sSessionID, IN long lStoreServiceId, IN long lDiskId, OUT long* plIsUse);
/*
功  能：获取存储录像设备状态
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lStoreServiceId：存储服务器id
	  pstStoreStaus:返回存储设备通道录像状态信息
	  plReCount:返回存储设备通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetStoreDeviceStatus(IN char* sSessionID,IN long lStoreServiceId,OUT LPICMS_STORE_STATUS pstStoreStaus,OUT long* plReCount);
/*****************存储方案**************************/
/*
功  能：获取存储方案列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPageInfo：分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstStoreStrategy:返回存储策略列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetStoreStrategyList(IN char* sSessionID,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_STORE_STRATEGY pstStoreStrategy);

/*
功  能：增加存储方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  pstStoreChann：存储通道列表
	  lChannCount:通道个数
	  stStoreStrategy:存储策略信息
	  plStrategyId:返回存储策略id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddStoreStrategyItem(IN char* sSessionID,IN ICMS_STORE_STRATEGY stStoreStrategy,IN LPICMS_CHANN pstStoreChann,IN long lChannCount,OUT long* plStrategyId);

/*
功  能：删除整个存储方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lStrategyId:存储策略id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelStoreStrategyItem(IN char* sSessionID,IN long lStrategyId);

/*
功  能：删除某个存储方案的某个通道
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lStrategyId:存储策略id
	  stStoreChann:存储通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelStoreStrategyChann(IN char* sSessionID,IN long lStrategyId,IN ICMS_CHANN stStoreChann);

/*
功  能：增加某个存储方案的通道
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lStrategyId:存储策略id
	  pstStoreChann:存储通道列表
	  lChannCount：存储通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddStoreStrategyChann(IN char* sSessionID,IN long lStrategyId,IN LPICMS_CHANN pstStoreChann,IN long lChannCount);

/*
功  能：修改存储方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lStrategyId:存储策略id
	  stStoreStrategy:存储策略信息
	  pstStoreChann:存储通道信息
	  lChannCount：存储通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyStoreStrategyItem(IN char* sSessionID,IN long lStrategyId,IN ICMS_STORE_STRATEGY stStoreStrategy,IN LPICMS_CHANN pstStoreChann,IN long lChannCount);

/*
功  能：查询存储方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lStrategyId:存储策略id
	  pstStoreStrategy:返回存储策略信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetStoreStrategyItem(IN char* sSessionID,IN long lStrategyId,OUT LPICMS_STORE_STRATEGY pstStoreStrategy);

/*
功  能：查询某个方案存储通道
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lStrategyId:存储策略id
	  stPageInfo:分页请求信息
      pstRspPageInfo:分页响应信息
	  pstChann:返回存储通道列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetStoreStrategyChann(IN char* sSessionID,IN long lStrategyId,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_CHANN pstChann);
/*****************云台业务**************************/
/*
功  能：打开云台独占
参  数：
	  sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stGldata:设备节点信息
	  pstHoldUser：返回独占用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZOpenHoldEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lChannel, OUT LPICMS_HOLD_USER pstHoldUser);
/*
功  能：云台控制
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lChannel:设备通道
      lCmd:控制命令
	  lParam:控制参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZControl(IN char* sSessionID,IN long lDeviceId,IN long lChannel,IN  long lCmd,IN  long lParam);

/*
功  能：云台控制扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lChannel:设备通道
	lCmd:控制命令
	lParam:控制参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZControlEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lChannel, IN  long lCmd, IN  long lParam);
/*
功  能：云台控制独占
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lDeviceId:设备id
      lChannel:设备通道
      lCmd:控制命令
      lParam:控制参数
	  pstHoldUser：返回独占用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZControlHold(IN char* sSessionID, IN long lDeviceId, IN long lChannel, IN  long lCmd, IN  long lParam, OUT LPICMS_HOLD_USER pstHoldUser);
/*
功  能：云台控制独占扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lChannel:设备通道
	lCmd:控制命令
	lParam:控制参数
	pstHoldUser：返回独占用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZControlHoldEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lChannel, IN  long lCmd, IN  long lParam, OUT LPICMS_HOLD_USER pstHoldUser);

/*
功  能：设置预置位
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lChannel:设备通道
      lIndex:预置位
	  bSet:是否设置
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZPreSet(IN char* sSessionID,IN long lDeviceId,IN long lChannel,IN long lIndex,IN bool bSet);
/*
功  能：设置预置位扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lChannel:设备通道
	lIndex:预置位
	bSet:是否设置
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZPreSetEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lChannel, IN long lIndex, IN bool bSet);
/*
功  能：设置预置位独占
参  数：
		sSessionID:用户会话ID，由ICMS_UserLogin返回
		lDeviceId:设备id
		lChannel:设备通道
		lIndex:预置位
		bSet:是否设置
		pstHoldUser：返回独占用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZPreSetHold(IN char* sSessionID, IN long lDeviceId, IN long lChannel, IN long lIndex, IN bool bSet, OUT LPICMS_HOLD_USER pstHoldUser);
/*
功  能：设置预置位独占扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lChannel:设备通道
	lIndex:预置位
	bSet:是否设置
	pstHoldUser：返回独占用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZPreSetHoldEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lChannel, IN long lIndex, IN bool bSet, OUT LPICMS_HOLD_USER pstHoldUser);

/*
功  能：停止云台独占
参  数：
		sSessionID:用户会话ID，由ICMS_UserLogin返回
		lDeviceId:设备id
		lChannel:设备通道
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZStopHold(IN char* sSessionID, IN long lDeviceId, IN long lChannel);
/*
功  能：停止云台独占扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lChannel:设备通道
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PTZStopHoldEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lChannel);
/*****************语音对讲和广播**************************/
/*
功  能：打开语音对讲
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenVoiceTalk(IN char* sSessionID,IN long lDeviceId);
/*
功  能：打开语音对讲扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenVoiceTalkEx(IN char* sSessionID, IN ICMS_GLDATA stGldata);
/*
功  能：打开语音对讲独占
参  数：
		sSessionID:用户会话ID，由ICMS_UserLogin返回
		lDeviceId:设备id
		pstHoldUser：返回独占用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenVoiceTalkHold(IN char* sSessionID, IN long lDeviceId,OUT LPICMS_HOLD_USER pstHoldUser);
/*
功  能：打开语音对讲独占扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	pstHoldUser：返回独占用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenVoiceTalkHoldEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, OUT LPICMS_HOLD_USER pstHoldUser);
/*
功  能：关闭语音对讲
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CloseVoiceTalk(IN char* sSessionID,IN long lDeviceId);
/*
功  能：关闭语音对讲扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CloseVoiceTalkEx(IN char* sSessionID, IN ICMS_GLDATA stGldata);
/*
功  能：关闭语音对讲独占
参  数：
		sSessionID:用户会话ID，由ICMS_UserLogin返回
		lDeviceId:设备id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CloseVoiceTalkHold(IN char* sSessionID, IN long lDeviceId);
/*
功  能：关闭语音对讲独占扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CloseVoiceTalkHoldEx(IN char* sSessionID, IN ICMS_GLDATA stGldata);
/*
功  能：打开语音广播
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenBroadCast(IN char* sSessionID,IN long lDeviceId);
/*
功  能：打开语音广播扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenBroadCastEx(IN char* sSessionID, IN ICMS_GLDATA stGldata);
/*
功  能：关闭语音广播
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CloseBroadCast(IN char* sSessionID,IN long lDeviceId);
/*
功  能：关闭语音广播扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CloseBroadCastEx(IN char* sSessionID, IN ICMS_GLDATA stGldata);
/*****************轮询业务**************************/
/*
功  能：查询轮询组方案列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPageInfo:分页请求信息
      pstRspPageInfo:分页响应信息
	  pstCycleGroup:返回轮询方案列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetCycleGroupList(IN char* sSessionID,IN ICMS_QUERY_PAGEINFO stPageInfo, OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_CYCLEGROUP_INFO pstCycleGroup);

/*
功  能：增加轮询组方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stCycleGroup:轮询方案信息
	  plCycleGroupId:返回轮询方案id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddCycleGroup(IN char* sSessionID,IN ICMS_CYCLEGROUP_INFO stCycleGroup,OUT long *plCycleGroupId);

/*
功  能：增加轮询段方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lCycleGroupId:轮询方案id
	  stCustomCycle:轮询段信息
	  pstChann:轮询通道列表
	  lChanCount:轮询通道个数
	  plCustomCycleId:返回轮询段id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddCustomCycle(IN char *sSessionID,IN long lCycleGroupId,IN ICMS_CUSTOMCYCLE_INFO stCustomCycle,IN OUT LPICMS_CHANN pstChann,IN long lChanCount,OUT long *plCustomCycleId);

/*
功  能：修改轮询段信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lCycleGroupId:轮询方案id
	  stCustomCycle:轮询段信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyCustomCycle(IN char *sSessionID,IN long lCycleGroupId,IN ICMS_CUSTOMCYCLE_INFO stCustomCycle);

/*
功  能：修改轮询段窗口
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lCycleGroupId:轮询方案id
	  pstWnd:轮询窗口信息
	  lWndCount:轮询通道列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyCycleWnd(IN char *sSessionID,IN long lCycleGroupId,IN LPICMS_WND pstWnd,IN long lWndCount);

/*
功  能：添加轮询段通道
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lCycleGroupId:轮询方案id
	  pstChann:轮询通道信息
	  lChannCount:轮询通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddCycleChann(IN char *sSessionID,IN long lCycleGroupId,IN LPICMS_CHANN pstChann,IN long lChannCount);

/*
功  能：删除轮询组方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lCycleGroupId:轮询方案id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelCycleGroup(IN char* sSessionID,IN long lCycleGroupId);

/*
功  能：删除轮询段方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lCycleGroupId:轮询方案id
	  pCustomCycleId:轮询段id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelCustomCycle(IN char* sSessionID,IN long lCycleGroupId,IN long pCustomCycleId);

/*
功  能：删除轮询某段某个通道
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lCycleGroupId:轮询方案id
	  pCustomCycleId:轮询段id
	  stChann:轮询通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelCustomCycleChann(IN char* sSessionID,IN long lCycleGroupId,IN long pCustomCycleId,IN ICMS_CHANN stChann);

/*
功  能：获取某个轮询方案信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lCycleGroupId:轮询方案id
	  pstCycleGroup:返回轮询方案信息
	  pstCustomCycle:返回轮询段列表
	  plCustomCycleCount:轮询段个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetCycleGroup(IN char* sSessionID,IN long lCycleGroupId,OUT LPICMS_CYCLEGROUP_INFO pstCycleGroup,OUT LPICMS_CUSTOMCYCLE_INFO pstCustomCycle,OUT long* plCustomCycleCount);

/*
功  能：获取轮询段通道列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lCycleGroupId:轮询方案id
	  lCustomCycleId:轮询段id
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstChann:返回轮询通道列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetCustomCycleChann(IN char* sSessionID,IN long lCycleGroupId,IN long lCustomCycleId,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_CHANN pstChann);

//启动轮询
//ICMS_API long __stdcall ICMS_StartCycle(IN char* sSessionID,IN long lCycleGroupId,ICMS_HWND stHwnd);

//关闭轮询
//ICMS_API long __stdcall ICMS_StopCycle(IN char* sSessionID,IN long lCycleGroupId);
/*****************报警业务**************************/
//设置报警类型--没有报警类型表
//获取报警类型

/*
功  能：获取报警方案列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstAlarmScheme:返回报警方案列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAlarmSchemeList(IN char* sSessionID,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_ALARM_SCHEME pstAlarmScheme);

/*
功  能：获取报警方案信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id
	  pstAlarmScheme:返回报警方案信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAlarmSchemeInfo(IN char* sSessionID,IN long lSchemeId,OUT LPICMS_ALARM_SCHEME pstAlarmScheme);

/*
功  能：增加报警联动方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stAlarmScheme:报警方案信息
	  plSchemeId:返回报警方案id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddAlarmScheme(IN char* sSessionID,IN ICMS_ALARM_SCHEME stAlarmScheme,OUT long* plSchemeId);

/*
功  能：修改报警联动方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id
	  stAlarmScheme:报警方案信息  
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyAlarmScheme(IN char* sSessionID,IN long lSchemeId,IN ICMS_ALARM_SCHEME stAlarmScheme);

/*
功  能：删除报警联动方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelAlarmScheme(IN char* sSessionID,IN long lSchemeId);

/*
功  能：批量增加报警来源
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  pstAlarmSource:报警来源列表-多项
	  lReCount:报警来源个数
返回值：错误码
注  释:pstAlarmSource返回各个源id*/
ICMS_API long __stdcall ICMS_AddAlarmSource(IN char* sSessionID,IN long lSchemeId,IN OUT LPICMS_ALARM_SOURCE pstAlarmSource,IN long lReCount);

/*
功  能：修改报警来源
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  stAlarmSource:报警来源
返回值：错误码
注  释:只修改轮询间隔和窗口行列*/
ICMS_API long __stdcall ICMS_ModifyAlarmSource(IN char* sSessionID,IN long lSchemeId,IN ICMS_ALARM_SOURCE stAlarmSource);

/*
功  能：获取某个报警方案报警来源列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstAlarmSource:报警来源列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAlarmSourceList(IN char* sSessionID,IN long lSchemeId,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_ALARM_SOURCE pstAlarmSource);

/*
功  能：删除某个报警来源
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  lAlarmSourceId:报警来源id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelAlarmSource(IN char* sSessionID,IN long lSchemeId,IN long lAlarmSourceId);

/*
功  能：获取某个报警来源的关联视频设备列表信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  lAlarmSourceId:报警来源id
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstAlarmLinkVideo:关联视频列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAlarmLinkVideoList(IN char* sSessionID,IN long lSchemeId,IN long lAlarmSourceId,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_ALARM_LINK_VIDEO pstAlarmLinkVideo);

/*
功  能：增加关联视频设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  lAlarmSourceId:报警来源id
	  pstAlarmLinkVideo:关联视频设备通道列表
	  lReCount:关联视频设备通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddAlarmLinkVideo(IN char* sSessionID,IN long lSchemeId,IN long lAlarmSourceId,IN OUT LPICMS_ALARM_LINK_VIDEO pstAlarmLinkVideo,IN long lReCount);

/*
功  能：删除关联视频设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  lAlarmSourceId:报警来源id
	  stAlarmLinkVideo:关联视频设备通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelAlarmLinkVideo(IN char* sSessionID,IN long lSchemeId,IN long lAlarmSourceId,IN ICMS_ALARM_LINK_VIDEO stAlarmLinkVideo);

/*
功  能：修改关联视频设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  lAlarmSourceId:报警来源id
	  pstAlarmLinkVideo:关联视频设备通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyAlarmLinkVideo(IN char* sSessionID,IN long lSchemeId,IN long lAlarmSourceId,IN LPICMS_ALARM_LINK_VIDEO pstAlarmLinkVideo);

/*
功  能：获取某个报警来源的关联输出设备列表信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  lAlarmSourceId:报警来源id
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstAlarmLinkOut:关联输出设备通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAlarmLinkOutList(IN char* sSessionID,IN long lSchemeId,IN long lAlarmSourceId,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_ALARM_LINK_OUT pstAlarmLinkOut);

/*
功  能：增加关联输出设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  lAlarmSourceId:报警来源id
	  pstAlarmLinkOut:关联输出设备通道信息列表
	  lReCount:关联输出设备通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddAlarmLinkOut(IN char* sSessionID,IN long lSchemeId,IN long lAlarmSourceId,IN LPICMS_ALARM_LINK_OUT pstAlarmLinkOut,IN long lReCount);

/*
功  能：删除关联输出设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  lAlarmSourceId:报警来源id
	  stAlarmLinkOut:关联输出设备通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelAlarmLinkOut(IN char* sSessionID,IN long lSchemeId,IN long lAlarmSourceId,IN ICMS_ALARM_LINK_OUT stAlarmLinkOut);

/*
功  能：修改关联输出设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lSchemeId:报警方案id 
	  lAlarmSourceId:报警来源id
	  pstAlarmLinkOut:关联输出设备通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyAlarmLinkOut(IN char* sSessionID,IN long lSchemeId,IN long lAlarmSourceId,IN LPICMS_ALARM_LINK_OUT pstAlarmLinkOut);

/*
功  能：获取节假日设置列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPageInfo:分页请求信息 
	  pstRspPageInfo:分页响应信息
	  pstHoliday:节假日信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetHolidayList(IN char* sSessionID,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_HOLIDAY pstHoliday);

/*
功  能：新增节假日
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stHoliday:节假日信息
	  plHolidyaId:返回节假日id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddHoliday(IN char* sSessionID,IN ICMS_HOLIDAY stHoliday,OUT long* plHolidyaId);

/*
功  能：修改节假日项
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stHoliday:节假日信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyHoliday(IN char* sSessionID,IN ICMS_HOLIDAY stHoliday);

/*
功  能：删除节假日
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lHolidyaId:节假日id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DeleteHoliday(IN char* sSessionID,IN long lHolidyaId);

/*
功  能：清除重复节假日
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ClearRepeatHoliday(IN char* sSessionID);

/*
功  能：清除失效节假日
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ClearInvalidHoliday(IN char* sSessionID);


//*********报警联动********************************************************/
/*
功  能：注册报警视频上报
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  fImmediaAlarmCallBack:回调函数
	       sSessionID:用户会话ID，由ICMS_UserLogin返回
	       lReErr:返回错误码
	       pstImmediateAlarm:报警信息
	       dwUser:用户参数
	  dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RegisterImmediaAlarm(IN char* sSessionID,IN void (CALLBACK *fImmediaAlarmCallBack) (char* sSessionID ,long lReErr,LPICMS_IMMEDIATE_ALARM pstImmediateAlarm, DWORD dwUser),IN DWORD dwUser);

/*
功  能：获取报警图片
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  sPath:路径
	  sSerial:报警序列号
	  fAlarmPhotoCallBack:回调函数
	       sSessionID:用户会话ID，由ICMS_UserLogin返回
	       lReErr:返回错误码
	       sPhotoBuf:图片数据
		   lBufSize:图片长度
	       dwUser:用户参数
	  dwUser:用户参数
返回值：错误码
注  释:lReErr为小于0时为错误码，大于等于为图片个数,等于0为图片获取完成*/
ICMS_API long __stdcall ICMS_GetAlarmPhoto(IN char* sSessionID,IN char* sPath,IN char* sSerial,IN void (CALLBACK *fAlarmPhotoCallBack) (char* sSessionID ,long lReErr,unsigned char* sPhotoBuf,long lBufSize, DWORD dwUser),IN DWORD dwUser);
//注册报警短信上报

//注册报警信息上报

//******************************************************报警日志管理******************************************//
/*
功  能：修改报警等级
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  sSerial:报警序列号
	  lAlarmLevel:报警等级，1-低，2-中，3-高
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyAlarmLevel(IN char* sSessionID,IN char* sSerial,IN long lAlarmLevel);
/*
功  能：处理报警
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  sSerial:报警序列号
	  sDealMsg:处理信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_UpdateAlarmDealMsg(IN char* sSessionID,IN char* sSerial,IN char* sDealMsg);

/*
功  能：查询报警信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stAlarmMng:请求报警信息
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstAlarmMngRe:返回报警信息
	  pstTime:返回有日志天信息列表，最多查询365天
	  plReCount:返回天数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAlarmMngList(IN char* sSessionID,IN ICMS_ALARM_MNG stAlarmMng,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_ALARM_MNG_RE pstAlarmMngRe,OUT LPICMS_TIME pstTime,OUT long* plReCount);

/*
功  能：删除报警信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stAlarmMng:请求报警信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelAlarmMng(IN char* sSessionID,IN ICMS_ALARM_MNG stAlarmMng);
//导出
//ICMS_API long __stdcall ICMS_ExportAlarmMng(IN char* sSessionID);
/************************************************************解码服务器******************************/
/*
功  能：获取解码服务器下的解码设备列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstDecodeData:返回管理设备列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetDecodeDeviceList(IN char *sSessionID, IN long lServerId,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_DECODEDEVICE pstDecodeDevice);

/*
功  能：增加解码设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  stDecodeDevice:解码设备信息
	  plDeviceId:解码设备id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddDecodeDevice(IN char *sSessionID, IN long lServerId,IN ICMS_DECODEDEVICE stDecodeDevice,IN long *plDeviceId);

/*
功  能：获取解码设备信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  pstDecodeDevice:返回解码设备信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetDecodeDevice(IN char *sSessionID, IN long lServerId,IN long lDeviceId,OUT LPICMS_DECODEDEVICE pstDecodeDevice);

/*
功  能：修改解码设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  lDeviceId:解码设备id
	  stDecodeDevice:解码设备信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyDecodeDevice(IN char *sSessionID, IN long lServerId,IN long lDeviceId,IN ICMS_DECODEDEVICE stDecodeDevice);

/*
功  能：删除解码设备
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  lDeviceId:解码设备id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelDecodeDevice(IN char *sSessionID, IN long lServerId,IN long lDeviceId);

/*
功  能：获取解码设备类型
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  pstDecodeType:解码设备类型
	  lReCount:解码设备类型实际个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetDecodeDeviceType(IN char *sSessionID, IN long lServerId,OUT LPICMS_DECODEDEVICE_TYPE pstDecodeType,OUT long* plReCount);


/*
功  能：注册设备连接状态回调
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId：服务器id
	  fDecodeDeviceStatusCallBack:回调函数
	           sSessionID:用户会话ID，由ICMS_UserLogin返回
			   lDeviceId:设备id
			   lcode:日志代码
			   lConErr:错误号
			   dwUser:用户参数
	  dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RegisterDecodeDeviceStatus(IN char* sSessionID, IN long lServerId,IN void (CALLBACK *fDecodeDeviceStatusCallBack)(char* sSessionID,long lDeviceId,long lcode,long lConErr,DWORD dwUser),IN DWORD dwUser);

/*
功  能：注销解码设备连接状态回调
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:服务器id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_UnRegisterDecodeDeviceStatus(IN char* sSessionID, IN long lServerId);
//搜索解码设备
ICMS_API long __stdcall ICMS_SearchDecodeDevice(IN char *sSessionID,OUT LPICMS_DECODEDEVICE pstDecodeDevice,OUT long lReCount);
/*******************************************************电视墙****************************************/

/*
功  能：获取默认报警上墙方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  plCycleGroupId:返回报警上墙方案id
返回值：错误码
注  释:*/
//ICMS_API long __stdcall ICMS_GetDefaultAlarmTvWallGroup(IN char* sSessionID, IN long lServerId,OUT long *plCycleGroupId);

/*
功  能：获取报警上墙段列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  pstCustomCycle:返回报警上墙段列表
	  plCustomCycleCount:返回报警上墙段个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAlarmTvWallSegList(IN char* sSessionID,IN long lServerId,OUT LPICMS_CUSTOMCYCLE_INFO pstCustomCycle,OUT long* plCustomCycleCount);

/*
功  能：增加报警上墙段
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  stCustomCycle:报警上墙段信息
	  plCustomCycleId:返回报警上墙段id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddAlarmTvWallSeg(IN char *sSessionID,IN long lServerId,IN ICMS_CUSTOMCYCLE_INFO stCustomCycle,OUT long *plCustomCycleId);

/*
功  能：删除报警上墙段
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  lCustomCycleId:报警上墙段id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelAlarmTvWallSeg(IN char* sSessionID,IN long lServerId,IN long lCustomCycleId);

/*
功  能：获取报警上墙段窗口信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lServerId:解码服务器id
	  lCustomCycleId:报警上墙段id
	  pstWnd:返回报警上墙段窗口信息
	  plWndCount:返回报警上墙窗口个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAlarmTvWalSeglWnd(IN char *sSessionID,IN long lServerId,IN long lCustomCycleId,OUT LPICMS_WND pstWnd,OUT long* plWndCount);

/*
功  能：修改报警上墙段信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  stCustomCycle:报警上墙段信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyAlarmTvWalSeg(IN char *sSessionID,IN long lServerId,IN ICMS_CUSTOMCYCLE_INFO stCustomCycle);

/*
功  能：修改报警上墙段窗口
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  pstWnd:报警上墙窗口信息
	  lWndCount:报警上墙窗口个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyAlarmTvWallWnd(IN char *sSessionID,IN long lServerId,IN LPICMS_WND pstWnd,IN long lWndCount);

/*
功  能：获取默认轮询上墙方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  plCycleGroupId:返回轮询上墙方案id
返回值：错误码
注  释:*/
//ICMS_API long __stdcall ICMS_GetDefaultCycleTvWallGroup(IN char* sSessionID,IN long lServerId,OUT long *plCycleGroupId);

/*
功  能：获取轮询上墙段列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  pstCustomCycle:返回轮询上墙段信息
	  plCustomCycleCount:返回轮询上墙段个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetCycleTvWallSegList(IN char* sSessionID,IN long lServerId,OUT LPICMS_CUSTOMCYCLE_INFO pstCustomCycle,OUT long* plCustomCycleCount);

/*
功  能：增加轮询上墙段方案
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  stCustomCycle:轮询上墙段信息
	  pstChann:轮询上墙通道列表
	  lChanCount:轮询上墙通道个数
	  plCustomCycleId:返回轮询上墙段id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddCycleTvWallSeg(IN char *sSessionID,IN long lServerId,IN ICMS_CUSTOMCYCLE_INFO stCustomCycle,IN LPICMS_CHANN pstChann,IN long lChanCount,OUT long *plCustomCycleId);

/*
功  能：删除轮询上墙段
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  lCustomCycleId:轮询上墙段id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelCycleTvWallSeg(IN char* sSessionID,IN long lServerId,IN long lCustomCycleId);

/*
功  能：获取轮询上墙段窗口信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  lCustomCycleId:轮询上墙段id
	  pstWnd:返回轮询上墙窗口信息
	  plWndCount:返回轮询上墙窗口个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetCycleTvWallSegWnd(IN char *sSessionID,IN long lServerId,IN long lCustomCycleId,OUT LPICMS_WND pstWnd,OUT long* plWndCount);

/*
功  能：修改轮询上墙段信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  stCustomCycle:轮询上墙段信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyCycleTvWallSeg(IN char *sSessionID,IN long lServerId,IN ICMS_CUSTOMCYCLE_INFO stCustomCycle);

/*
功  能：修改轮询上墙段窗口
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  pstWnd:轮询上墙窗口信息
	  lWndCount:轮询上墙段窗口个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyCycleTvWallSegWnd(IN char *sSessionID,IN long lServerId,IN LPICMS_WND pstWnd,IN long lWndCount);

/*
功  能：获取轮询上墙段通道列表
参  数：
		sSessionID:用户会话ID，由ICMS_UserLogin返回
		lServerId:解码服务器id
		lCustomCycleId:轮询段id
		stPageInfo:分页请求信息
		pstRspPageInfo:分页响应信息
		pstChann:返回轮询通道列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetCycleTvWallSegChann(IN char* sSessionID,IN long lServerId,IN long lCustomCycleId,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_CHANN pstChann);

/*
功  能：删除轮询上墙某段某个通道
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  lCustomCycleId:轮询段id
	  stChann:轮询通道信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelCycleTvWallSegChann(IN char* sSessionID,IN long lServerId,IN long lCustomCycleId,IN ICMS_CHANN stChann);

/*
功  能：增加轮询上墙段通道
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lServerId:解码服务器id
	  pstChann:轮询上墙通道信息
	  lChannCount:轮询上墙通道个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddCycleTvWallSegChann(IN char *sSessionID,IN long lServerId,IN LPICMS_CHANN pstChann,IN long lChannCount);

//************************************级联平台管理**************************************************/
/*
功  能：获取代理服务器列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPageInfo:请求页信息
	  pstRspPageInfo:返回相应页信息
	  pstProServer:返回代理服务器列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetProxyServerList(IN char* sSessionID,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_PROXY_SERVER pstProServer);

/*
功  能：获取级联平台列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lProxyId:代理服务器id,默认为1
	  stPageInfo:请求页信息
	  pstRspPageInfo:返回相应页信息
	  pstServer:返回级联平台列表
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetPsList(IN char* sSessionID,IN long lProxyId,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_PS_SERVER pstServer);

/*
功  能：添加级联平台
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPs:级联平台信息
	  plPsId:返回级联平台id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddPs(IN char* sSessionID,IN ICMS_PS_SERVER stPs,OUT long* plPsId);
/*
功  能：添加级联平台(内蒙古电力定制）
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     stPs:级联平台信息
     plPsId:返回级联平台id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddPsSpec(IN char* sSessionID, IN ICMS_PS_SERVER_SPEC stPs, OUT long* plPsId);
/*
功  能：修改级联平台连接配置和平台资源配置
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPs:级联平台信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyPs(IN char* sSessionID,IN ICMS_PS_SERVER stPs);
/*
功  能：修改级联平台(内蒙古电力定制）
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     stPs:级联平台信息
	 plPsId:返回级联平台id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyPsSpec(IN char* sSessionID, IN ICMS_PS_SERVER_SPEC stPs, OUT long* plPsId);
/*
功  能：获取级联平台信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lPsId:级联平台id
	  pstPs:返回级联平台信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetPsInfo(IN char* sSessionID,IN long lPsId,OUT LPICMS_PS_SERVER pstPs);

/*
功  能：获取级联平台id（内蒙古电力定制）
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     psOuoppKeyId:级联平台主键编号
     pstPs:返回级联平台id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetPsInfoSpec(IN char* sSessionID, IN char* psOuoppKeyId, OUT long* plPsId);

/*
功  能：删除级联平台
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lPsId:级联平台id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelPs(IN char* sSessionID,IN long lPsId); 
/*
功  能：删除级联平台(内蒙古电力定制)
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
	 psOuoppKeyId:级联平台主键编号
     plPsId:返回级联平台id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelPsSpec(IN char* sSessionID, IN char* psOuoppKeyId, OUT long* plPsId);


/*
功  能：注册级联平台连接状态
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  fPsStatusCallBack:
	            sSessionID:用户会话ID，由ICMS_UserLogin返回
				bIsOnline:级联平台在线状态,0-不在线，1-在线
				stGlData:级联平台信息
				dwUser:用户参数
	  dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RegisterPsServerStatus(IN char* sSessionID, IN void (CALLBACK *fPsStatusCallBack) (char* sSessionID, bool bIsOnline, ICMS_GLDATA stGlData, DWORD dwUser), IN DWORD dwUser);

/*
功  能：取消注册级联平台连接状态
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_UnRegisterPsServerStatus(IN char* sSessionID);
/*********************************************************电子地图**************************************/
/*
功  能：获取所有图层节点列表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stPageInfo:请求页信息
	  pstRspPageInfo:返回相应页信息
	  pstMap:返回图层信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetLayerMapTree(IN char* sSessionID,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_MAP pstMap);

/*
功  能：获取某图层图片和设备节点
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lLayerId:图层id
	  sMapFile:返回文件路径带文件名
	  plFileLen:返回文件路径长度
      pstLayerNode:返回图层节点
	  plLayerNodeLen://返回图层节点个数
	  pstDeviceNode://返回设备节点
	  plDeviceNodeLen://返回设备节点个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetLayerMap(IN char* sSessionID,IN long lLayerId,OUT char* sMapFile,OUT long* plFileLen,OUT LPICMS_LAYER_NODE pstLayerNode,OUT long* plLayerNodeLen,OUT LPICMS_DEVICE_NODE pstDeviceNode,OUT long* plDeviceNodeLen);

/*
功  能：添加图层
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lParentLayerId:图层父id
	  lLayerType:图层图片类型,0是jpg，1是shp
	  sLayerName:图层名称
      sMapFilePath:图层图片带文件名路径
	  plLayerId:返回图层id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddLayerMap(IN char* sSessionID,IN long lParentLayerId,IN long lLayerType,IN char* sLayerName,IN char* sMapFilePath,OUT long* plLayerId);

/*
功  能：修改图层
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lLayerId:图层id
	  lLayerType:图层图片类型,0是jpg，1是shp
	  sLayerName:图层名称
      sMapFilePath:图层图片带文件名路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyLayerMap(IN char* sSessionID,IN long lLayerId,IN long lLayerType,IN char* sLayerName,IN char* sMapFilePath);

/*
功  能：删除图层
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lLayerId:图层id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelLayerMap(IN char* sSessionID,IN long lLayerId);

/*
功  能：图层添加修改下级节点
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stLowNode:图层节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddLayerLowNode(IN char* sSessionID,IN ICMS_LAYER_NODE stLowNode);

/*
功  能：图层添加修改设备节点
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stDeviceNode:设备节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddLayerDeviceNode(IN char* sSessionID,IN ICMS_DEVICE_NODE stDeviceNode);

/*
功  能：获取下级图层节点属性
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lPosX:鼠标点击x坐标
	  lPosY:鼠标点击y坐标
	  lWidth:图标宽
	  lHeight:图标高
	  pstLowNode:返回图层节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetLayerLowNode(IN char* sSessionID,IN long lPosX,IN long lPosY,IN long lWidth,IN long lHeight,OUT LPICMS_LAYER_NODE pstLowNode);

/*
功  能：获取设备节点属性
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lPosX:鼠标点击x坐标
	  lPosY:鼠标点击y坐标
	  lWidth:图标宽
	  lHeight:图标高
	  pstDeviceNode:返回设备节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetLayerDeviceNode(IN char* sSessionID,IN long lPosX,IN long lPosY,IN long lWidth,IN long lHeight,OUT LPICMS_DEVICE_NODE pstDeviceNode);

/*
功  能：删除节点(使用坐标sdk内部判断是设备、通道或者图层节点)
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lPosX:鼠标点击x坐标
	  lPosY:鼠标点击y坐标
	  lWidth:图标宽
	  lHeight:图标高
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelNode(IN char* sSessionID,IN long lPosX,IN long lPosY,IN long lWidth,IN long lHeight);

/*
功  能：删除设备节点
参  数：
		sSessionID:用户会话ID，由ICMS_UserLogin返回
		stDeviceNode:设备节点信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelLayerDeviceNode(IN char* sSessionID, IN ICMS_DEVICE_NODE stDeviceNode);

/*
功  能：删除图层节点
参  数：
        sSessionID:用户会话ID，由ICMS_UserLogin返回
        lLayerId:图层节点id
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelLayerLowNode(IN char* sSessionID, IN long lLayerId);

/*****************升级管理**************************/
//上传文件
//下载文件
//启动升级
/*****************事件管理**************************/
//设置事件类型
//获取事件类型
/*
功  能：注册事件信息上报
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  fImmediaAlarmCallBack:回调函数
	       sSessionID:用户会话ID，由ICMS_UserLogin返回
	       lReErr:返回错误码
	       pstImmediateAlarm:报警信息
	       dwUser:用户参数
	  dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RegisterSysEventAlarm(IN char* sSessionID,IN void (CALLBACK *fSysEventCallBack) (char* sSessionID ,long lReErr,LPICMS_LOG_MNG_RE pstSysEvent, DWORD dwUser),IN DWORD dwUser);

//查询事件
//打印事件
/*****************日志管理**************************/
/*
功  能：写日志
参  数：
	  sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lLogCode:日志代码
	  lObjectType:日志对象类型
	  lEventId:事件id
	  lError:错误码
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_WriteLog(IN char* sSessionID, IN long lLogCode, IN long lObjectType, IN long lEventId, IN long lError);
//设置日志类型
//获取日志类型

/*
功  能：查询系统日志信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stLogMng:请求日志信息，查询整个一级的日志和设备区域无关
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstLogMngRe:返回日志信息
	  pstTime:返回有日志天信息列表，最多查询365天
      plReCount:返回天数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetLogMngList(IN char* sSessionID,IN ICMS_LOG_MNG stLogMng,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_LOG_MNG_RE pstLogMngRe,OUT LPICMS_TIME pstTime,OUT long* plReCount);

/*
功  能：删除系统日志信息
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stLogMng:请求日志信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelLogMng(IN char* sSessionID,IN ICMS_LOG_MNG stLogMng);
//导出日志

//************************************状态显示***************************************************/
/*
功  能：视频信号状态
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stGlData:区域节点
      pstVideoSig:返回多项设备信号状态
      plReCount://返回设备个数
返回值：错误码
注  释:*/
//ICMS_API long __stdcall ICMS_GetVideoSignalStatus(IN char* sSessionID,IN ICMS_GLDATA stGlData,OUT LPICMS_STATUS_SIGNAL pstVideoSig,OUT long* plReCount);
ICMS_API long __stdcall ICMS_GetVideoSignalStatus(IN char* sSessionID,IN ICMS_GLDATA stGlData,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT  LPICMS_STATUS_SIGNAL pstVideoSig);
/*
功  能：设备磁盘状态
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  fDiskStatusCallBack:回调函数
	        sSessionID:用户会话ID
			lReErr:错误码
            pstDisk:返回多个磁盘状态
            lReCount://返回磁盘个数
		    dwUser:用户参数
	  dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetDeviceDiskStatus(IN char* sSessionID,IN long lDeviceId,IN void (CALLBACK *fDiskStatusCallBack) (char* sSessionID ,long lReErr,OUT LPICMS_DEVICE_DISK pstDisk,OUT long lReCount,DWORD dwUser),IN DWORD dwUser);
/*
功  能：设备磁盘状态扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	fDiskStatusCallBack:回调函数
	sSessionID:用户会话ID
	lReErr:错误码
	pstDisk:返回多个磁盘状态
	lReCount://返回磁盘个数
	dwUser:用户参数
	dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetDeviceDiskStatusEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN void (CALLBACK *fDiskStatusCallBack) (char* sSessionID, long lReErr, OUT LPICMS_DEVICE_DISK pstDisk, OUT long lReCount, DWORD dwUser), IN DWORD dwUser);

/*
功  能：移动侦测状态
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stGlData:区域节点
      pstMotion:返回多项设备移动侦测状态
      plReCount://返回设备个数
返回值：错误码
注  释:*/
//ICMS_API long __stdcall ICMS_GetDeviceMotionStatus(IN char* sSessionID,IN ICMS_GLDATA stGlData,OUT LPICMS_STATUS_SIGNAL pstMotion,OUT long* plReCount);
ICMS_API long __stdcall ICMS_GetDeviceMotionStatus(IN char* sSessionID,IN ICMS_GLDATA stGlData,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT  LPICMS_STATUS_SIGNAL pstMotion);

/*
功  能：报警输入状态
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stGlData:区域节点
      pstAlarmIn:返回多项设备报警输入状态
      plReCount://返回设备个数
返回值：错误码
注  释:*/
//ICMS_API long __stdcall ICMS_GetAlarmInStatus(IN char* sSessionID,IN ICMS_GLDATA stGlData,OUT LPICMS_STATUS_SIGNAL pstAlarmIn,OUT long* plReCount);
ICMS_API long __stdcall ICMS_GetAlarmInStatus(IN char* sSessionID,IN ICMS_GLDATA stGlData,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT  LPICMS_STATUS_SIGNAL pstAlarmIn);

/*
功  能：报警输出状态
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  stGlData:区域节点
      pstAlarmOut:返回多项设备报警输出状态
      plReCount://返回设备个数
返回值：错误码
注  释:*/
//ICMS_API long __stdcall ICMS_GetAlarmOutStatus(IN char* sSessionID,IN ICMS_GLDATA stGlData,OUT LPICMS_STATUS_SIGNAL pstAlarmOut,OUT long* plReCount);
ICMS_API long __stdcall ICMS_GetAlarmOutStatus(IN char* sSessionID,IN ICMS_GLDATA stGlData,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT  LPICMS_STATUS_SIGNAL pstAlarmOut);

//温湿度状态
//**************************************通道分组管理*********************************************/

//************************************数据库管理**************************************************/
/*
功  能：获取数据库端口
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     pPsPath:下级路径
     plDbPort:返回数据库端口
返回值：错误码
注  释: */
ICMS_API long __stdcall ICMS_GetDatabasePort(IN char* sSessionID, IN char* pPsPath, OUT long* plDbPort);
/*
功  能：打开数据库
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  sDbName: 要打开的数据库的名字，如果数据库不存在则创建此数据库
	  pPsPath:下级路径
      plHandleDb:返回打开的数据库句柄	 	  
返回值：错误码
注  释: 目前暂不支持创建新数据库，使用服务器已有的数据库*/
ICMS_API long __stdcall ICMS_OpenDatabase(IN char* sSessionID, IN char* sDbName,IN char* pPsPath, OUT long* plHandleDb);

/*
功  能：执行Sql语句,可以对数据库进行操作，包括建表、删除表、更新表内容等操作
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lHandleDb： 数据库句柄
      sSql:要执行的sql语句,组织sql语句字段应转为utf8
      pPsPath:下级路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ExecuteSql(IN char* sSessionID, IN long lHandleDb, IN char* sSql, IN char* pPsPath);

/*
功  能：执行Sql语句,可以对数据库进行操作，包括建表、删除表、更新表内容等操作(扩展）
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lHandleDb： 数据库句柄
      sSql:要执行的sql语句,组织sql语句字段应转为utf8
	  pPsPath:下级路径
	  plId:返回最后插入项的ID
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ExecuteSqlEx(IN char* sSessionID,IN long lHandleDb,IN char* sSql,IN char* pPsPath,OUT long* plId);

/*
功  能：关闭数据库
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lHandleDb:数据库句柄	  
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CloseDatabase(IN char* sSessionID, IN long lHandleDb);

/*
功  能：通过sql语句打开数据表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lHandleDb:数据库句柄
	  sSql: 查询语句
	  plHandleRS：返回打开的数据表句柄
	  pPsPath:下级路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_OpenRecordSet(IN char* sSessionID,IN long lHandleDb,IN char* sSql,IN char* pPsPath, OUT long* plHandleRS);

/*
功  能：判断数据表是否到末尾
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lHandleRS:数据表句柄
返回值：错误码
注  释:*/
ICMS_API BOOL __stdcall ICMS_RecordSetIsEOF(IN char* sSessionID, IN long lHandleRS);

/*
功  能：移动到下一条记录
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lHandleRS:数据表句柄  
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RecordSetMoveNext(IN char* sSessionID, IN long lHandleRS);

/*
功  能：通过索引获取当前字段的内容
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lHandleRS:数据表句柄
	  index: 索引号
返回值：错误码
注  释:*/
ICMS_API const char* __stdcall ICMS_RecordSetIndexGetValue(IN char* sSessionID, IN long lHandleRS, IN int index);

/*
功  能：通名称获取当前字段的内容
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lHandleRS:数据表句柄
	  sFiled:字段名称
返回值：错误码
注  释:*/
ICMS_API const char* __stdcall ICMS_RecordSetFiledGetValue(IN char* sSessionID, IN long lHandleRS, IN char* sFiled);

/*
功  能：关闭数据表
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lHandleRS:数据表句柄
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CloseRecordSet(IN char* sSessionID, IN long lHandleRS);

/************************************************人脸识别**************************************************/
/*
功  能：增加图片库
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      stFacePic:人脸图片信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_AddFacePic(IN char* sSessionID,IN LPICMS_FACE_PIC pstFacePic);

/*
功  能：修改图片库
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      stFacePic:人脸图片信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyFacePic(IN char* sSessionID,IN LPICMS_FACE_PIC pstFacePic);

/*
功  能：删除图片库
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      stFacePic:人脸图片信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelFacePic(IN char* sSessionID,IN char* psPeopleId,IN long lPicId);

/*
功  能：获取图片
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      lPeopleId:人id
      lPicId:图片id
      stFacePic:人脸图片信息
	  plPicCount:返回图片总数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetFacePic(IN char* sSessionID,IN char* psPeopleId,IN long lPicId,OUT LPICMS_FACE_PIC pstFacePic,OUT long* plPicCount);

/*
功  能：查询人脸识别日志
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      stFaceAlarm:人脸识别查询条件
	  stPageInfo:分页请求信息
	  pstRspPageInfo:分页响应信息
	  pstFaceAlarmRe：返回人脸识别日志
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetFaceAlarmList(IN char* sSessionID,IN ICMS_FACE_ALARM stFaceAlarm,IN ICMS_QUERY_PAGEINFO stPageInfo,OUT LPICMS_RSP_PAGEINFO pstRspPageInfo,OUT LPICMS_FACE_ALARM_RE pstFaceAlarmRe);

/*
功  能：处理人脸识别日志
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      sLogId:人脸识别日志id
	  sDealMsg:处理信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DealFaceAlarm(IN char* sSessionID,IN char* psLogId,IN char* sDealMsg);

/*
功  能：注册人脸识别报警上传
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  fFaceRecognitionCallBack:回调函数
	       sSessionID:用户会话ID，由ICMS_UserLogin返回
	       lReErr:返回错误码
	       stFaceRecognition:人脸识别信息
	       dwUser:用户参数
	  dwUser:用户参数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_RegisterFaceRecognition(IN char* sSessionID,IN void (CALLBACK *fFaceRecognitionCallBack) (char* sSessionID ,long lReErr,LPICMS_FACE_ALARM_RE pstFaceAlarmRe, DWORD dwUser),IN DWORD dwUser);

/*
功  能：注销人脸识别报警上传注册
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_UnRegisterFaceRecognition(IN char* sSessionID);

/*
功  能：是否开启获取人脸识别参数
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
	  lDeviceId:设备id
	  lStreamHandle:视频流id
      bIsOpen:是否开启显示人脸osd，默认关闭
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_IsOpenFaceRecognitionOsd(IN char* sSessionID,IN long lDeviceId,IN long lStreamHandle,IN BOOL bIsOpen = TRUE);

/*
功  能：是否开启获取人脸识别参数扩展
参  数：
	sSessionID:用户会话ID，由ICMS_UserLogin返回
	stGldata:设备节点信息
	lStreamHandle:视频流id
	bIsOpen:是否开启显示人脸osd，默认关闭
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_IsOpenFaceRecognitionOsdEx(IN char* sSessionID, IN ICMS_GLDATA stGldata, IN long lStreamHandle, IN BOOL bIsOpen = TRUE);
//***************************************************字符串转换*****************************************************/
/*
功  能：ASCII转utf8
参  数：
     sAscii:字符串
返回值：utf8格式字符串
注  释:*/
ICMS_API const char* __stdcall ICMS_StringAsciiToUtf8(char* sAscii);

/*
功  能：获取服务器系统时间
参  数：
     pstSysTime:服务器系统时间
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetServerSysTime(IN char* sSessionID, OUT LPICMS_TIME pstSysTime);
//***************************************************虚拟用户*****************************************************/

/*
功  能：同步虚拟用户
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
	 psOuoppKeyId:级联服务器主键编号
     stVirtual:虚拟用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PushVirtualUser(IN char* sSessionID, IN char* psOuoppKeyId, IN ICMS_VIRTUAL_USER stVirtual);

/*
功  能：同步虚拟用户扩展
参  数：
    sSessionID:用户会话ID，由ICMS_UserLogin返回
    pPsPath:级联服务器路径
    stVirtual:虚拟用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PushVirtualUserEx(IN char* sSessionID, IN char* psOuoppKeyId, IN char* pPsPath, IN ICMS_VIRTUAL_USER stVirtual);

/*
功  能：修改虚拟用户
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     psOuoppKeyId:级联服务器主键编号
     stVirtual:虚拟用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyVirtualUser(IN char* sSessionID, IN char* psOuoppKeyId, IN ICMS_VIRTUAL_USER stVirtual);

/*
功  能：修改虚拟用户扩展
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     pPsPath:级联服务器路径
     stVirtual:虚拟用户信息
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_ModifyVirtualUserEx(IN char* sSessionID, IN char* psOuoppKeyId, IN char* pPsPath, IN ICMS_VIRTUAL_USER stVirtual);

/*
功  能：删除虚拟用户
参  数：
      sSessionID:用户会话ID，由ICMS_UserLogin返回
      psOuoppKeyId:级联服务器主键编号
      sUserName:虚拟用户账号
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelVirtualUser(IN char* sSessionID, IN char* psOuoppKeyId, IN char* sUserName);

/*
功  能：删除虚拟用户扩展
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     pPsPath:级联服务器路径
     sUserName:虚拟用户账号
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_DelVirtualUserEx(IN char* sSessionID, IN char* psOuoppKeyId, IN char* pPsPath, IN char* sUserName);

/*
功  能：获取级联公开角色
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     psOuoppKeyId:级联服务器主键编号
     pstPostInfo:返回角色信息
	 plCount:返回角色个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAllRole(IN char* sSessionID, IN char* psOuoppKeyId, OUT LPICMS_POST_INFO pstPostInfo, OUT long* plCount);

/*
功  能：获取级联公开组织机构和角色
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     psOuoppKeyId:级联服务器主键编号
	 stPageInfo:分页请求信息
	 pstRspPageInfo:分页响应信息
	 pOrgRole:所有组织机构和角色，多项
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAllOrgRole(IN char* sSessionID, IN char* psOuoppKeyId,IN char* pVirtualName, IN  ICMS_QUERY_PAGEINFO stPageInfo, OUT LPICMS_RSP_PAGEINFO pstRspPageInfo, OUT LPICMS_ORG_ROLE pOrgRole);

/*
功  能：获取级联公开组织机构和角色扩展
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     pPsPath:级联服务器路径
     stPageInfo:分页请求信息
     pstRspPageInfo:分页响应信息
     pOrgRole:所有组织机构和角色，多项
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetAllOrgRoleEx(IN char* sSessionID, IN char* psOuoppKeyId, IN char* pPsPath, IN char* pVirtualName, IN  ICMS_QUERY_PAGEINFO stPageInfo, OUT LPICMS_RSP_PAGEINFO pstRspPageInfo, OUT LPICMS_ORG_ROLE pOrgRole);

/*
功  能：绑定级联虚拟用户和公开角色
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     psOuoppKeyId:级联服务器主键编号
     sUserName:虚拟用户名称
     sPostKeyId:角色id，多个用“，”隔开，例如“1,2,3”
	 iCount:绑定角色个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PushRoleBand(IN char* sSessionID, IN char* psOuoppKeyId, IN char* sUserName, IN char* sPostKeyId, IN int iCount);

/*
功  能：绑定级联虚拟用户和公开角色扩展
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     pPsPath:级联服务器路径
     sUserName:虚拟用户名称
     sPostKeyId:角色id，多个用“，”隔开，例如“1,2,3”
     iCount:绑定角色个数
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_PushRoleBandEx(IN char* sSessionID, IN char* psOuoppKeyId, IN char* pPsPath, IN char* sUserName, IN char* sPostKeyId, IN int iCount);


/*
功  能：获取资源通道列表
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
     stGlData:区域或级联服务器信息
	 pResourceType：资源类型
	 stPageInfo:分页请求信息
	 pstRspPageInfo:分页响应信息
	 pstDeviceData:返回通道信息，多项
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_GetResourceDeviceItem(IN char *sSessionID, IN ICMS_GLDATA stGlData, IN char* pResourceType,IN  ICMS_QUERY_PAGEINFO stPageInfo, OUT LPICMS_RSP_PAGEINFO pstRspPageInfo, OUT LPICMS_DLData pstDeviceData);

/*
功  能：判断功能权限
参  数：
     sSessionID:用户会话ID，由ICMS_UserLogin返回
	 phandleLable：操作权限定义
     pPsPath:级联路径
返回值：错误码
注  释:*/
ICMS_API long __stdcall ICMS_CheckHandleAuthority(IN char *sSessionID, IN char *phandleLable, IN  char* pPsPath);


#endif  // HB_PLATSDK_H