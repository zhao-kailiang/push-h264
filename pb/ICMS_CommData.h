//#pragma pack(4)
#pragma once
//应用长度
#define ICMS_MIN_LEN                     32     //最小长度
#define ICMS_COMM_LEN0                   36     //通用长度0
#define ICMS_COMM_LEN                    64     //通用长度
#define ICMS_MAX_LEN                     1024   //最大长度
#define ICMS_MAX_NUM                     128    //最大个数
#define ICMS_NAME_LEN                    256    //名称长度
#define ICMS_SESSION_MAX_LEN             256    //会话id长度
#define ICMS_RIGHT_MAX_LEN               256    //最大权限长度
#define ICMS_MAX_CHANNUM                 128    //最大通道个数
#define ICMS_MAX_WND_NUM                 64     //最大窗口数
#define ICMS_MAX_CYCLE_NUM               64     //最大轮询段数
#define ICMS_MAX_WEEK_DAY		         8		//星期
#define ICMS_MAX_TIME_SEG		         8		//时间段
#define ICMS_MAX_DISK_COUNT              128    //最大盘符个数
#define ICMS_ARRAY_LEN                   256    //数组长度
#define ICMS_MAX_BLOCK_AREA		         4		//遮挡区域
#define ICMS_PIC_MAX_LEN                 512*1024 //图片最大长度
//设备树状态标志位
#define ICMS_DL_NSF_ONLINE    0x01
#define ICMS_DL_NSF_EXPAND    0x02

//设备类型		
#define ICMS_DEVICE_NAME_HB            1   //DVR/NVR/IPC
#define ICMS_DEVICE_NAME_IPC_ADDED     2   //IPC_YCX
#define ICMS_DEVICE_NAME_HIK           10  //海康设备
#define ICMS_DEVICE_NAME_DAHUA         20  //大华设备
#define ICMS_DEVICE_NAME_IPC_RDZC      30  //IPC_锐达智诚
#define ICMS_DEVICE_NAME_IPC_ONVIF     40  //ONVIF设备
#define ICMS_DEVICE_NAME_ECHO          50  //ECHO设备
#define ICMS_DEVICE_NAME_JABSCO        60  //捷高设备
#define ICMS_DEVICE_NAME_SONY          70  //索尼摄像机
#define ICMS_DEVICE_NAME_HQ            80  //海清摄像机
#define ICMS_DEVICE_NAME_NA            90  //尼得摄像机
#define ICMS_DEVICE_NAME_BTX           100 //亚科德摄像机
#define ICMS_DEVICE_NAME_HB_INITIATIVE  1001//DVR/NVR/IPC 主动连接

//区域节点变更类型
#define ICMS_NOTIFY_APPEND        1 //添加
#define ICMS_NOTIFY_DELETE        2 //删除
#define ICMS_NOTIFY_MODIFY        3 //修改

//播放模式
#define ICMS_STREAM_NONE                0           //无流缓冲控制策略
#define ICMS_STREAM_REALTIME_PRIORITY   1          //实时性优先
#define ICMS_STREAM_BALANCED            2          //均衡
#define ICMS_STREAM_FLUENCY_PRIORITY    3          //流畅性优先

//回放速度
#define ICMS_BACKWARD_SPEED			   -66

//报警上传状态---无状态信号/突发事件使用SIGNAL_STATUS_SINGLE(如设备离线), 有持续性状态的使用后面3种
#define ICMS_SIGNAL_STATUS_SINGLE           1
#define ICMS_SIGNAL_STATUS_BEGIN            2
#define ICMS_SIGNAL_STATUS_UPDATE           3
#define ICMS_SIGNAL_STATUS_END              4
#define ICMS_SIGNAL_STATUS_COMBIN           5

//默认窗口个数
#define ICMS_DEFAULT_SUB_WINDOW_ROW		2

//用户权限
#define ICMS_RIGHT_ATOM_ALL				"255"	//拥有所用权限
#define ICMS_RIGHT_ATOM_ROLE			"1"		//用户角色管理
#define ICMS_RIGHT_ATOM_USER			"2"		//用户管理
#define ICMS_RIGHT_ATOM_REGION			"3"		//区域管理
#define ICMS_RIGHT_ATOM_DEVICE			"4"		//设备管理
#define ICMS_RIGHT_ATOM_CHANNEL			"5"		//设备通道管理
#define ICMS_RIGHT_ATOM_SERVER		    "6"		//服务器管理
#define ICMS_RIGHT_ATOM_ALARM_OUT		"7"		//报警输出
#define ICMS_RIGHT_ATOM_STORE			"8"		//存储管理
#define ICMS_RIGHT_ATOM_MAP				"9"		//电子地图管理
#define ICMS_RIGHT_ATOM_SYS_PARAM		"10"	//系统参数配置
#define ICMS_RIGHT_ATOM_EVENT_RECEIVE	"11"	//事件接收
#define ICMS_RIGHT_ATOM_SYS_BACKUP		"12"	//系统备份/恢复
#define ICMS_RIGHT_ATOM_REALTIME_PLAY  	"13"	//实时播放
#define ICMS_RIGHT_ATOM_REALTIME_PARAM	"14"	//视频参数设置
#define ICMS_RIGHT_ATOM_PTZ_PARAM		"15"	//云台参数设置
#define ICMS_RIGHT_ATOM_PTZ_CTRL		"16"	//云台控制
#define ICMS_RIGHT_ATOM_REALTIME_CYCLE	"17"	//实时轮训管理
#define ICMS_RIGHT_ATOM_CAPTURE			"18"	//抓图
#define ICMS_RIGHT_ATOM_LOCAL_STORE		"19"	//本地录像
#define ICMS_RIGHT_ATOM_HISTROY_PLAY	"20"	//历史播放
#define ICMS_RIGHT_ATOM_HISTROY_DOWNLOAD "21"	//历史下载
#define ICMS_RIGHT_ATOM_HISTROY_DELETE	"22"	//历史删除
#define ICMS_RIGHT_ATOM_DECODE_PLAY		"23"	//解码服务关联
#define ICMS_RIGHT_ATOM_DECODE_CYCLE	"24"	//电视墙管理
#define ICMS_RIGHT_ATOM_ALARM_LOG		"25"	//报警日志管理
#define ICMS_RIGHT_ATOM_ALARM_INSTANT	"26"	//即时报警
#define ICMS_RIGHT_ATOM_TALK			"27"	//语音对讲
#define ICMS_RIGHT_ATOM_BROADCAST		"28"	//语音广播
#define ICMS_RIGHT_ATOM_CTRL_DVR		"29"	//控制DVR重启、校时等
#define ICMS_RIGHT_ATOM_PUSH_TASK        "30"	//信息发布
#define ICMS_RIGHT_ATOM_CASCADE          "31"    //级联管理
#define ICMS_RIGHT_ATOM_ALARM_SET        "32"    //报警方案配置
#define ICMS_RIGHT_ATOM_MAINTENANCE      "33"    //运维管理
#define ICMS_RIGHT_ATOM_KEYBOARD_ENCODE  "34"    //键盘编码
#define ICMS_RIGHT_ATOM_MAP_GIS          "35"    //GIS地图
#define ICMS_RIGHT_ATOM_MAP_3D           "36"    //3D地图
#define ICMS_RIGHT_ATOM_MAP_3D_CONFIG    "37"    //3D配置
#define ICMS_RIGHT_ATOM_COMPANY_MNG      "38"    //企业管理

//操作权限定义(内蒙电力定制)
#define ICMS_RIGHT_ATOM_DEVICE_SEARCH_SPEC     "DeviceManagement_manage"//设备管理
#define ICMS_RIGHT_ATOM_STORE_SPEC			"ServerManagement_store"//存储管理
#define ICMS_RIGHT_ATOM_MEDIA_STREAM_SPEC			"ServerManagement_stream"		//流媒体管理
#define ICMS_RIGHT_ATOM_SERVER_SPEC			"ServerManagement_server"		//服务器管理
#define ICMS_RIGHT_ATOM_MAP_SPEC				"ElectronicMap_electronicMap"		//电子地图管理
#define ICMS_RIGHT_ATOM_EVENT_RECEIVE_SPEC			"VideoEvent_template"	//事件模板管理
#define ICMS_RIGHT_ATOM_EVENT_SEARCH_SPEC     "VideoEvent_search"  //事件检索回放
#define ICMS_RIGHT_ATOM_EVENT_CREATE_SPEC     "VideoEvent_creatEvent"//事件生产
#define ICMS_RIGHT_ATOM_EVENT_RECORDSTATUS_SPEC  "VideoEvent_recordState"//事件录像状态
#define ICMS_RIGHT_ATOM_REALTIME_PLAY_SPEC	"Preview_preview"	//实时播放
#define ICMS_RIGHT_ATOM_PTZ_CTRL_SPEC		"Preview_ptz"	//云台控制
#define ICMS_RIGHT_ATOM_REALTIME_CYCLE_SPEC	"Preview_round"	//实时轮训管理
#define ICMS_RIGHT_ATOM_LOCAL_STORE_SPEC		"Playback_record"	//本地录像
#define ICMS_RIGHT_ATOM_HISTROY_PLAY_SPEC		"Playback_playback"	//历史查询回放
#define ICMS_RIGHT_ATOM_HISTROY_DOWNLOAD_SPEC	"Playback_download"	//历史下载
#define ICMS_RIGHT_ATOM_LOG_SPEC		           "LogManagement_log"	//日志查询
#define ICMS_RIGHT_ATOM_STRATEGY_SEARCH_SPEC	  "AlarmManagement_manage"//报警方案管理
#define ICMS_RIGHT_ATOM_FACE_MANAGE_SPEC      "FaceManagement_face"//人脸库管理
#define ICMS_RIGHT_ATOM_TALK_SPEC				"Preview_talk"	//语音对讲
#define ICMS_RIGHT_ATOM_STATUS_SHOW_SPEC      "StatusDisplay_statusDisplay"//状态显示


//资源类型(内蒙电力定制)
#define ICMS_RESOUCE_TYPE_REALTIME_PLAY      "tb_preview_channel"   //视频预览
#define	ICMS_RESOUCE_TYPE_PTZ                "tb_ptz_channel"        //云台控制	
#define	ICMS_RESOUCE_TYPE_OPENVOICE          "tb_intercom_channel"	//语音对讲	
#define	ICMS_RESOUCE_TYPE_PLAYBACK           "tb_playback_channel"   //	查询回放	
#define	ICMS_RESOUCE_TYPE_DOWNLOAD           "tb_download_channel"	//录像下载	
#define	ICMS_RESOUCE_TYPE_STORE              "tb_storage_management" //	存储管理	
#define	ICMS_RESOUCE_TYPE_MEDIA              "tb_streaming_media_channel" //	流媒体管理
//限时类型
#define ICMS_RIGHT_LIMIT_NONE                0   //不限时
#define ICMS_RIGHT_LIMIT_TIME                1   //按时间限时
#define ICMS_RIGHT_LIMIT_DATE                2   //按日期限时

//录像类型
#define ICMS_REC_TYPE_HAND		 1			//手动录像
#define ICMS_REC_TYPE_TIMING	 2			//定时录像
#define ICMS_REC_TYPE_MD		 3			//移动侦测录像
#define ICMS_REC_TYPE_ALARM		 4			//报警录像
#define ICMS_REC_TYPE_CARDCOR    5			//通道卡号录像
#define ICMS_REC_TYPE_HISBACKUP  0xfe       //平台历史备份
#define ICMS_REC_TYPE_ALL		 0xff		//所有类型录像

//服务器类型。
#define ICMS_SERVICE_MEDIA     0               //流媒体服务器
#define ICMS_SERVICE_STORE     1               //存储服务器
#define ICMS_SERVICE_DECODE    2               //解码服务器

//存储服务器类型
#define ICMS_STORE_NORMAL_TYPE    1
#define ICMS_STORE_CVR_TYPE       2
//报警等级
#define ICMS_ALARM_LOW         1//低
#define ICMS_ALARM_MID         2//中
#define ICMS_ALARM_HIGH        3//高
//报警类型
#define ICMS_ALARM_NONE			0		//未知
#define ICMS_ALARM_IN_TYPE		1		//探头报警
#define ICMS_ALARM_MD_TYPE		2		//移动侦测报警
#define ICMS_ALARM_VIDEO_LOST	3		//视频丢失报警
#define ICMS_ALARM_DISK_ERR		4		//磁盘异常报警
#define ICMS_ALARM_VCOVER_TYPE	5		//视频遮挡报警-----20110908
#define ICMS_ALARM_STORE_ERR		   6		//磁盘录像覆盖故障报警
#define ICMS_ALARM_DEVICE_ERR	       7		//主机故障报警
#define ICMS_ALARM_INTELLIGENT_TYPE    8		//智能设备报警
#define ICMS_ALARM_RECORD_NOTENOUGH    9		//主机录像天数不足
#define ICMS_ALARM_DEVICE_TEMPERATURE  10		//主机温度过高
#define ICMS_ALARM_FACE_TYPE            16     //人脸报警 
#define ICMS_ALARM_DEVICE_OFF	       1000	//设备离线报警----该报警消息由平台自动产生
#define ICMS_ALARM_DEVICE_STORE_NOT_ENOUGH	1001	 //设备录像不足报警----该报警消息由平台自动产生
#define ICMS_ALARM_FIRE_PLATFORM            1201     //消防平台报警
#define ICMS_ALARM_110_DEVICE               1002     //110设备报警
#define ICMS_ALARM_GENERAL_DEVICE	        2000	 //报警主机
//报警主机类型
#define ICMS_ALARM_DEVICE_ZONE_TYPE_0		2001	//有线防区报警
#define ICMS_ALARM_DEVICE_ZONE_TYPE_1		2002	//无线防区报警
#define ICMS_ALARM_DEVICE_ZONE_TYPE_2		2003	//无线紧急报警
#define ICMS_ALARM_DEVICE_ZONE_TYPE_3		2004	//键盘紧急
#define ICMS_ALARM_DEVICE_ZONE_TYPE_4		2005	//键盘防拆
#define ICMS_ALARM_DEVICE_ZONE_TYPE_5		2006	//有线防区延时报警
#define ICMS_ALARM_DEVICE_ZONE_TYPE_6		2007	//无线防区延时报警
#define ICMS_ALARM_DEVICE_ZONE_TYPE_7		2008	//总线防区
#define ICMS_ALARM_DEVICE_ZONE_TYPE_8		2009	//内置防区
#define ICMS_ALARM_DEVICE_ZONE_TYPE_9		2010	//火警
//云台参数
#define MAX_PROTOCOL		64		//协议长度

//云台方向
#define ICMS_PTZ_LEFT		0x00000001	//向左旋转
#define ICMS_PTZ_RIGHT		0x00000002	//向右旋转
#define ICMS_PTZ_UP			0x00000003	//向上旋转
#define ICMS_PTZ_DOWN		0x00000004	//向下旋转
#define ICMS_PTZ_LEFT_UP	0x00000005	//向左上角旋转
#define ICMS_PTZ_LEFT_DOWN	0x00000006	//向左下角旋转
#define ICMS_PTZ_RIGHT_UP	0x00000007	//向右上角旋转
#define ICMS_PTZ_RIGHT_DOWN	0x00000008	//向右下角旋转
#define ICMS_PTZ_ZOOM_MAX	0x00000009	//焦距放大
#define ICMS_PTZ_ZOOM_MIN	0x00000010	//焦距放小
#define ICMS_PTZ_FOCUS_FAR	0x00000011	//远焦点
#define ICMS_PTZ_FOCUS_NEAR	0x00000012	//近焦点
#define ICMS_PTZ_IRIS_OPEN	0x00000013	//光圈开
#define ICMS_PTZ_IRIS_CLOSE	0x00000014	//光圈关
#define ICMS_PTZ_LIGHT		0x00000015	//灯光
#define ICMS_PTZ_BRUSH		0x00000016	//雨刷
#define ICMS_PTZ_AUTO_MOVE	0x00000017	//自动扫描
#define ICMS_PTZ_STOP_MOVE	0x00000018	//停止扫描
#define ICMS_PTZ_STOP_FOCUS	0x00000019//停止聚焦 onvif协议需要区分

#define ICMS_PTZ_AUTO_CRUISE_START               0X0001002f //开启自动巡航
#define ICMS_PTZ_AUTO_CRUISE_STOP                0X00010030  //关闭自动巡航


//历史数据类型
enum ICMS_HisLogType{
	ICMS_His_Video = 0,
	ICMS_His_Pic,
};
//设备区域组织类型
typedef enum 
{
	ICMS_UNKNOWN_TYPE,
	ICMS_TYPE_REGION,          //区域
	ICMS_TYPE_PS,              //级联
	ICMS_TYPE_DEVICE,          //设备
	ICMS_TYPE_CHANNEL,         //通道
	ICMS_TYPE_GROUP,           //通道组
	ICMS_TYPE_GROUP_CHANNEL,   //分组通道
	ICMS_TYPE_SENSOR,          //报警输入
	ICMS_TYPE_ALARMOUT,        //报警输出
	ICMS_TYPE_WIDE_GROUP,      //通道分组(V2.0)
} NODETYPE;

#define ICMS_TYPE_STORE_SERVER    100 //存储服务器
#define ICMS_TYPE_STRATEGY        101 //存储策略

#define ICMS_TYPE_DECODE_SERVER   200 //解码服务器
#define ICMS_TYPE_DECODE_DEVICE   201 //解码设备
#define ICMS_TYPE_DECODE_CYCLE    202 //解码轮循方案
#define ICMS_TYPE_DECODE_CHANNEL  203 //解码通道

#define ICMS_TYPE_MEDIA_SERVER    500 //流媒体服务器

#define ICMS_TYPE_GENERAL_DEVICE_GLOBAL_PARAM  300 //全局参数修改
#define ICMS_TYPE_GENERAL_DEVICE    301	//通用报警设备
#define ICMS_TYPE_GENERAL_SENSOR	302	//报警主机报警输入

#define ICMS_TYPE_ALARM_STRATEGY    401 //报警方案
#define ICMS_TYPE_ALARM_MSG         402 //报警消息

//通道类型定义
#define ICMS_CHANNEL_TYPE_DEVICE		0	//设备
#define ICMS_CHANNEL_TYPE_VIDEO			1	//视频通道
#define ICMS_CHANNEL_TYPE_ALARM_IN		2	//报警输入通道
#define ICMS_CHANNEL_TYPE_ALARM_OUT		3	//报警输出通道
#define ICMS_CHANNEL_TYPE_DEC_OUT		4	//解码通道
//
#define ICMS_CHANNEL_TYPE_GENERAL_DEVICE		5	//报警设备
#define ICMS_CHANNEL_TYPE_GENERAL_DEV_AI		6	//报警设备报警输入通道

//帧类型
#define ICMS_VIDEO_I_FRAME			1			//关键帧(即:I帧)
#define ICMS_VIDEO_P_FRAME			2			//P帧
#define ICMS_VIDEO_B_FRAME			3			//B帧
#define ICMS_VIDEO_A_FRAME			4			//音频帧


//对象类型
#define ICMS_OBJ_TYPE_SERVER			1		//服务器
#define ICMS_OBJ_TYPE_CLIENT			2		//客户端
#define ICMS_OBJ_TYPE_ROLE			    3		//用户组
#define ICMS_OBJ_TYPE_USER			    4		//用户ID
#define ICMS_OBJ_TYPE_USER_LOGIN_ID	    5		//用户登陆ID
#define ICMS_OBJ_TYPE_DEVICE			6		//设备
#define ICMS_OBJ_TYPE_DEVICE_CHANNEL	7		//设备视频通道
#define ICMS_OBJ_TYPE_DEVICE_ALARM_IN_CHANNEL	8		//设备报警输入通道
#define ICMS_OBJ_TYPE_DEVICE_ALARM_OUT_CHANNEL	9		//设备报警输出通道
#define ICMS_OBJ_TYPE_REGION			10		//区域
#define ICMS_OBJ_TYPE_LOWER			    11		//下级平台
#define ICMS_OBJ_TYPE_GROUP			    12		//通道分组
#define ICMS_OBJ_TYPE_MEDIASERVER       13      //流媒体
#define ICMS_OBJ_TYPE_STORESERVER       14      //存储服务器
#define ICMS_OBJ_TYPE_DECODESERVER      15      //解码服务器
#define ICMS_OBJ_TYPE_DECODEDEVICE      16      //解码设备
#define ICMS_OBJ_TYPE_ALARM_DEVICE		17	    //报警设备
#define ICMS_OBJ_TYPE_EMAIL_ADDRESS				18	//用户邮件
#define ICMS_OBJ_TYPE_PUSH_TASK              19	//信息发布
#define ICMS_OBJ_TYPE_DISPLAY_PLAN           20	//显示方案
#define ICMS_OBJ_TYPE_COMPANY                21		//企业
#define ICMS_OBJ_TYPE_IPTALK_DEVICE          22
#define ICMS_OBJ_TYPE_IPTALK_PANEL           23

#define ICMS_OBJ_TYPE_EVENT                  100  //事件录像
//日志类型
#define ICMS_LOG_TYPE_OPERATOR		1		//操作日志
#define ICMS_LOG_TYPE_ERROR			2		//错误日志
#define ICMS_LOG_TYPE_DETECT		3		//运维日志
//日志代码
#define ICMS_LOG_CODE_SERVER_START			1		//服务启动
#define ICMS_LOG_CODE_SERVER_STOP			2		//服务停止
#define ICMS_LOG_CODE_USER_LOGIN			3		//用户登录
#define ICMS_LOG_CODE_USER_LOGOUT			4		//用户注销
#define ICMS_LOG_CODE_USER_CHANGED_PWD		5		//用户修改密码
#define ICMS_LOG_CODE_VERIFY_TIME			6		//设备校时
#define ICMS_LOG_CODE_REBOOT				7		//设备重启
#define ICMS_LOG_CODE_SHUTDOWN				8		//设备关机
#define ICMS_LOG_CODE_UPDATE				9		//设备升级
#define ICMS_LOG_CODE_SET_PIC_PARAM			10		//修改设备通道视频参数
#define ICMS_LOG_CODE_SET_COMPRESS_PARAM	11		//修改设备通道视频压缩参数
#define ICMS_LOG_CODE_SET_BLOCK_PARAM		12		//修改设备通道视频遮挡设置
#define ICMS_LOG_CODE_SET_OSD_PARAM			13		//修改设备通道视频OSD参数
#define ICMS_LOG_CODE_SET_RECORD_PARAM		14		//修改设备通道存储设置
#define ICMS_LOG_CODE_SET_MDBLOCK_PARAM		15		//修改设备通道移动侦测区域参数
#define ICMS_LOG_CODE_SET_MD_PARAM			16		//修改设备通道移动侦测布防时间参数
#define ICMS_LOG_CODE_SET_ALARM_IN_PARAM	17		//修改设备报警输入通道布防时间参数
#define ICMS_LOG_CODE_SET_ALARM_OUT_PARAM	18		//修改设备报警输出通道布防时间参数
#define ICMS_LOG_CODE_SET_PTZ_PARAM			19		//修改设备云台通道参数
#define ICMS_LOG_CODE_SYS_BACKUP			20		//系统备份
#define ICMS_LOG_CODE_SYS_RESTORE			21		//系统恢复

#define ICMS_LOG_CODE_SYS_REGISTER			30		//系统试用期即将到期

#define ICMS_LOG_CODE_APPEND                 101  //添加
#define ICMS_LOG_CODE_MODIFY                 102  //修改
#define ICMS_LOG_CODE_DELETE                 103  //删除
#define ICMS_LOG_CODE_OFFLINE                104  //离线
#define ICMS_LOG_CODE_ONLINE                 105  //在线
#define ICMS_LOG_CODE_RECONNECT              106  //重连上线

#define ICMS_LOG_CODE_CREAT_EVENT            200  //创建事件

#define ICMS_LOG_CODE_ADD_REGION			1000		//添加区域
#define ICMS_LOG_CODE_MODIFY_REGION			1001		//修改区域
#define ICMS_LOG_CODE_DELETE_REGION			1002		//删除区域
#define ICMS_LOG_CODE_ADD_ROLE				1003		//添加角色
#define ICMS_LOG_CODE_MODIFY_ROLE			1004		//修改角色
#define ICMS_LOG_CODE_DELETE_ROLE			1005		//删除角色
#define ICMS_LOG_CODE_ADD_USER				1006		//添加用户
#define ICMS_LOG_CODE_MODIFY_USER			1007		//修改用户
#define ICMS_LOG_CODE_DELETE_USER			1008		//删除用户
#define ICMS_LOG_CODE_RESET_USER			1009		//重置用户密码
#define ICMS_LOG_CODE_ADD_DEVICE			1010		//添加设备
#define ICMS_LOG_CODE_MODIFY_DEVICE			1011		//修改设备
#define ICMS_LOG_CODE_DELETE_DEVICE			1012		//删除设备
#define ICMS_LOG_CODE_MODIFY_DEVICE_CHANNEL	1013		//修改设备通道
#define ICMS_LOG_CODE_MODIFY_DEVICE_LINK	1014		//修改设备关联设置
#define ICMS_LOG_CODE_MODIFY_ARMING_TIME	1015		//修改布防时间段
#define ICMS_LOG_CODE_ADD_PS				1016		//添加下级平台
#define ICMS_LOG_CODE_MODIFY_PS				1017		//修改下级平台
#define ICMS_LOG_CODE_DELETE_PS				1018		//删除下级平台
#define ICMS_LOG_CODE_ADD_GROUP				1019		//添加通道分组
#define ICMS_LOG_CODE_MODIFY_GROUP			1020		//修改通道分组
#define ICMS_LOG_CODE_DELETE_GROUP			1021		//删除通道分组

//存储服务器错误日志
#define ICMS_LOG_CODE_STORE_ERROR_LOCAL_DB	5000		//本地数据库读写错误
#define ICMS_LOG_CODE_STORE_ERROR_DISK		5001		//存储磁盘(阵列)读写错误
#define ICMS_LOG_CODE_STORE_DISK_NO_SIZE	5002		//存储磁盘(阵列)空间不足
#define ICMS_LOG_CODE_ALARM_RECORD_START    5003        //启动报警存储
#define ICMS_LOG_CODE_ALARM_RECORD_STOP     5004        //结束报警存储

//解码设备类型
enum IcmsDev_Type
{
	ICMS_TYPE_1504DE = 1,//HB1504DE
	ICMS_TYPE_2208DE,    //HB2208DE
	ICMS_TYPE_2208DHE,   //HB2208DHE(高清)
	ICMS_TYPE_2301D,     //HB2301D
	ICMS_TYPE_2304D,     //HB2304D
	ICMS_TYPE_SCREEN,    //软解码主机
	ICMS_TYPE_NMCDECODER,//解码矩阵拼接服务器
	DECTYPE_COUNT
};
//历史视频下载状态
enum IcmsDownloadStatus{
	ICMS_DOWNLOAD_STATUS_RETRY = -2, //重试
	ICMS_DOWNLOAD_STATUS_FAIL = -1,  //失败
	ICMS_DOWNLOAD_STATUS_WAIT = 0,   //等待
	ICMS_DOWNLOAD_STATUS_ING	= 1, //正在下载
	ICMS_DOWNLOAD_STATUS_FINISH = 2, //完成
	ICMS_DOWNLOAD_STATUS_AVI = 3,    //转换avi
	ICMS_DOWNLOAD_STATUS_UNKNOW = 4, //未知
};
//图像格式-高四位为宽，第四位为高
#define ICMS_QCIF			0x00B00090		//QCIF   
#define ICMS_CIF			0x01600120		//CIF
#define ICMS_HD1			0x02C00120		//HD1
#define ICMS_D1				0x02C00240		//D1
#define ICMS_HD720P			0x050002D0		//HD720P
#define ICMS_HD1080P		0x07800438		//HD1080P
#define ICMS_H960H			0x03C00240		//960H
#define ICMS_Q960H			0x01E00120		//Q960H
#define ICMS_QQ960H		    0x00F00090		//QQ960H
#define ICMS_XX_720_576		0x02D00240
#define ICMS_XX_1280_1024	0x05000400
#define ICMS_XX_1280_960	0x050003C0
#define ICMS_XX_768_432		0x030001B0
#define ICMS_XX_640_368		0x02800170
#define ICMS_XX_320_192		0x014000C0
#define ICMS_XX_2048_1536   0x08000600
#define ICMS_XX_640_360     0x02800168

//星期
enum IcmsHolidays{
	ICMS_HOLIDAYS_SUN = 0,  //星期天
	ICMS_HOLIDAYS_MON,//星期一
	ICMS_HOLIDAYS_TUE,//星期二
	ICMS_HOLIDAYS_WED,//星期三
	ICMS_HOLIDAYS_THU,//星期四
	ICMS_HOLIDAYS_FRI,//星期五
	ICMS_HOLIDAYS_SAT,//星期六
	ICMS_HOLIDAYS_HOLD//节假日
};
//解码输出模式
enum ModeOutType{
	IMCS_MODEOUT_SD = 0,//SD
    IMCS_MODEOUT_720_50 ,//720P 50Hz
	IMCS_MODEOUT_720_60,//720P 60Hz
	IMCS_MODEOUT_1080_50,//1080P 50Hz
	IMCS_MODEOUT_1080_60//1080P 60Hz
};

//存储录像状态
enum RecordChStats{
	IMCS_Rcs_NoRecord = 0,//时间段外
	IMCS_Rcs_Recording,//录像中
	IMCS_Rcs_Error,//存储出错
	IMCS_Rcs_Count,
};
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// 参数配置结构体定义

//登陆返回结构体
typedef struct  
{
	char  sSessionID[ICMS_SESSION_MAX_LEN];		//用户会话id
	DWORD dwRoleID;					            //角色id
	char  sUserRight[ICMS_RIGHT_MAX_LEN];		//用户权限
	DWORD dwPlatformEdition;		 	     //平台版本
	DWORD dwUserRegion;                      //用户区域id
	char  sPsSerial[ICMS_COMM_LEN];           //平台序列号
	char  sPsName[ICMS_NAME_LEN];             //平台名称
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_LOGININFO, *LPICMS_LOGININFO;

//分页请求信息
typedef struct
{
	long lPageRowNum;//分页每页最大条数，1-200
	long lPageFirstRowNum;  //分页查询第一条的序号
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_QUERY_PAGEINFO,*LPICMS_QUERY_PAGEINFO;

//分页响应信息
typedef struct
{
	long lRowNum; //实际返回的条目数
	long lTotalRowNum; //符合条件的总条目数
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_RSP_PAGEINFO,*LPICMS_RSP_PAGEINFO;

//区域设备节点判断
typedef struct GLDATA
{
	char    sDevPath[ICMS_COMM_LEN];//设备路径，一级平台为空，下级平台不为空，根据规则写入平台id，若节点类型为通道则为:加设备id，如“:16”
	DWORD   dwID;   //设备或者区域id
	long    lNodeType;//设备区域组织类型
	bool operator==(const GLDATA &other) const
	{
		return ((this->lNodeType == other.lNodeType)
			&& (this->dwID == other.dwID)
			&& (strcmp(this->sDevPath,other.sDevPath) == 0));
	}
}ICMS_GLDATA,*LPICMS_GLDATA;

//设备列表节点数据
typedef struct	ICMS_DLData
{ 
	char    sDevPath[ICMS_COMM_LEN];//设备路径，一级平台为空，下级平台不为空，根据规则写入平台id，若节点类型为通道则为:加设备id，如“:16”
	long    lNodeType;//设备区域组织类型
	DWORD   dwParentId;//父id
	DWORD   dwID;   //本身id
	char    sRegionName[ICMS_NAME_LEN];//当为设备结构时，此项为上级区域名称,当为通道结构时，此为设备名称
	char    sName[ICMS_NAME_LEN];//本身名称
	long    lState; //[区域|分组]: 展开状态; [设备|通道|输入|输出]: 在线状态
	long    lChanCount;//当为设备时的通道个数
	long    lChanType;//通道类型
//	long    lProjectId;//项目工程id--只有设备的时候用
	char    sReserve[ICMS_COMM_LEN];//保留
	bool operator==(const ICMS_DLData& other) const
	{
		return ((this->lNodeType == other.lNodeType)
			&& (this->dwID == other.dwID)
			&& (strcmp(this->sDevPath, other.sDevPath) == 0))
			&& (this->dwParentId == other.dwParentId)
			&& (this->lChanType== other.lChanType);
	}
}ICMS_DLData,*LPICMS_DLData;

//通道信息
typedef struct  
{
	DWORD dwParentId;//所属父id,轮询通道时为轮询段id
	char sDevName[ICMS_NAME_LEN];//设备名
	char sChanName[ICMS_NAME_LEN];//通道名
	char  sDevPath[ICMS_COMM_LEN];//设备路径，一级平台为空，下级平台根据规则写入
	DWORD dwChanId;//通道id
	long  lStreamType;//流类型 0-录像流，1-网络流,256-自动检测
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_CHANN,*LPICMS_CHANN;

// 设备搜索
typedef enum
{
	SEARCH_HB = 0,//hb设备
	SEARCH_ONVIF,//onvif设备
	SEARCH_SONY,
	SEARCH_NMCNDM,
}ICMS_TYPE_SEARCH_DEV;

typedef struct 
{
	char szIP[ICMS_MIN_LEN];//设备ip
	long lPort;//设备端口
	long lChanCount;//通道个数
	char szType[ICMS_NAME_LEN];//设备型号类型
	char szName[ICMS_NAME_LEN];//名称
	char szMAC[ICMS_NAME_LEN];//mac地址
	char szMask[ICMS_NAME_LEN];//保留
	ICMS_TYPE_SEARCH_DEV eSearchType;//搜索类型
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_SEARCH_DEVICEINFO,*LPICMS_SEARCH_DEVICEINFO;
//时间
typedef struct{
	WORD dwYear;
	WORD dwMonth;
	WORD dwDay;
	WORD dwHour;
	WORD dwMinute;
	WORD dwSecond;
}ICMS_TIME,*LPICMS_TIME;
//设备结构体
typedef struct  
{
//	DWORD dwProjectId;      //项目id
	DWORD dwRegionId;		//区域id
	DWORD dwDeviceType;     //设备类型
	char sDeviceName[ICMS_COMM_LEN];					//设备名称
	char sDeviceDesc[ICMS_NAME_LEN];					//设备描述
	char sDeviceIp[ICMS_COMM_LEN];		 					//设备地址
	char sDeviceMac[ICMS_COMM_LEN];                      //mac地址
	DWORD dwDevicePort;                      //设备端口
	char sDeviceUserName[ICMS_COMM_LEN];              //用户名
	char sDevicePassword[ICMS_COMM_LEN];             // 密码
	DWORD dwChanNum;							//DVR 通道个数
	DWORD dwAlarmInNum;                   //报警输入个数
	DWORD dwAlarmOutNum;              //报警输出个数
	bool  IntelligentAlarm;             //是否自动开启智能报警
//	char sChanName[ICMS_MAX_CHANNUM][ICMS_COMM_LEN];  //通道名称
	char sSerialNum[ICMS_NAME_LEN];                        //序列号
	DWORD dwMedia;                           //所属流媒体
	DWORD dwAdjtimeType;                         //默认每天校时，上线校时
	char sAdjtimeData[ICMS_NAME_LEN];                         //按多少天校时
	DWORD dwDisableMedia;                   //禁止流媒体转发
	bool  bIsOnline;                        //是否在线
	long  lConnErr;                          //连接错误号
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_DEVICEINFO, *LPICMS_DEVICEINFO;

//视频图像参数
typedef struct
{
	long lBrightness;					//亮度
	long lContrast;						//对比度
	long lSaturation;					//饱和度
	long lHue;							//色调
}ICMS_VIDEO_PIC_PARAM, *LPICMS_VIDEO_PIC_PARAM;
//视频压缩参数范围
typedef struct  
{
	long arRangeStreamType[ICMS_ARRAY_LEN];//数组第一个值为实际种类个数，后面255个为数值(0-无音频流,1-有音频流)
	long arRangeFrameRate[ICMS_ARRAY_LEN];//数组第一个值为实际种类个数，后面255个为数值
	long arRangeBitRate[ICMS_ARRAY_LEN];//数组第一个值为实际种类个数，后面255个为数值,单位字节
	long arRangeBitRateMode[ICMS_ARRAY_LEN];//数组第一个值为实际种类个数，后面255个为数值(0-定码流,1-变码流)
	long arRangePicQuality[ICMS_ARRAY_LEN];//数组第一个值为实际种类个数，后面255个为数值
	long arRangeBitPicFormat[ICMS_ARRAY_LEN];//数组第一个值为实际种类个数，后面255个为数值
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_VIDEO_COMPRESS_RANGE,*LPICMS_VIDEO_COMPRESS_RANGE;
//视频压缩参数
typedef struct
{
	long lStreamType;					//码流类型(0-无音频流,1-有音频流)
	long lFrameRate;					//帧率
	long lIFrameInterval;				//I帧间隔
	long lBitRate;						//视频码率
	long lBitRateMode;					//码流模式(0-定码流,1-变码流)
	long lPicQuality;					//图像质量
	long lPicFormat;					//图像格式
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_VIDEO_COMPRESS_PARAM,*LPICMS_VIDEO_COMPRESS_PARAM;

//OSD参数
typedef struct
{
	long lLogoX;						//OSD文字X坐标
	long lLogoY;						//OSD文字Y坐标
	char szLogoName[ICMS_COMM_LEN];		//OSD文字
	long lShowLogo;						//0-不显示 1-显示 

	long lTimeX;						//时间X坐标
	long lTimeY;						//时间Y坐标
	long lFormat;						//时间格式
	long lShowTime;						//0-不显示 1-显示 
}ICMS_OSD_PARAM, *LPICMS_OSD_PARAM;

//视频遮挡区域
typedef struct
{
	LONG    left;
	LONG    top;
	LONG    right;
	LONG    bottom;
}ICMS_AREA_RECT, *LPICMS_AREA_RECT;
//视频遮挡区域组
typedef struct
{
	long lAreaCount;						//遮挡区域数
	ICMS_AREA_RECT arrBlockArea[ICMS_MAX_BLOCK_AREA];	//遮挡区域
}ICMS_VIDEO_BLOCK, *LPICMS_VIDEO_BLOCK;

typedef struct
{
	long lId[ICMS_ARRAY_LEN];           //0为无效，1为有效，当为通道时，0为不复制，1为复制
	long lReCount;                     //实际个数,当代表通道时根据设备通道个数赋值
}ICMS_ARRAY,*LPICMS_ARRAY;
typedef char	int8;
typedef short	int16;
typedef int		int32;

typedef unsigned char	uint8;
typedef unsigned short	uint16;
typedef unsigned int	uint32;
typedef unsigned __int64 uint64;

//回调数据结构体
typedef struct
{
	int frame_type;			    //帧类型 1：I帧 2：P帧 4：音频帧
	unsigned int	frame_num;  //帧号
	uint64			dwFrameTick;//视频帧时间戳
	unsigned int	width;	    //图像宽度
	unsigned int	height;	    //图像高度
	short			frame_rate; //帧率
	short			reserve1;	//保留
	int				reserve2;	//保留
}ICMS_FRAMEINFO,*LPICMS_FRAMEINFO;

//帧头信息
typedef struct 
{
	int8				szFlag[4];			//协议标记'PSVS'
	uint32				dwFrameOrder;		//视频贞序号
	uint64				dwFrameTick;		//视频帧时间戳
	int32				lFrameType;			//视频贞类型
	int32				lFrameSize;			//视频贞长度
	uint32				dwFrameDataCheck;	//视频贞数据校验码
	uint32				dwReserve;			//保留
//	uint8				frameData[0];		//视频贞数据
}ICMS_FRAME,*LPICMS_FRAME;

//区域信息
typedef struct
{
	long lParentId;           //父区域id
	char sRegionName[ICMS_NAME_LEN];     //区域名称
	char sRegionConnect[ICMS_COMM_LEN];  //联系人
	char sRegionPhone[ICMS_COMM_LEN];    //联系电话
	char sRegionAddress[ICMS_COMM_LEN];  //地址
	char sRegionRemark[ICMS_NAME_LEN];  //备注
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_REGIONINFO,*LPICMS_REGIONINFO;

//区域信息电力
typedef struct
{
	long lParentId;           //父区域id
	char sRegionName[ICMS_NAME_LEN];     //区域名称
	char sRegionConnect[ICMS_COMM_LEN];  //联系人
	char sRegionPhone[ICMS_COMM_LEN];    //联系电话
	char sRegionAddress[ICMS_COMM_LEN];  //地址
	char sRegionRemark[ICMS_NAME_LEN];  //备注
	char sBaseKeyId[ICMS_COMM_LEN];//组织机构id
	char sParentKeyId[ICMS_COMM_LEN];//组织机构父id
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_REGIONINFO_EX, *LPICMS_REGIONINFO_EX;

//权限
typedef struct  
{
// 	char sUserLoginId[ICMS_COMM_LEN];//
// 	long lRoleId;
	bool bIsUse;//是否应用
	BYTE bRightId;
	BYTE bRightType;//限时类型：0-不限时，1-按时间，2-按日期
	long lRightDay;//按天数
	ICMS_TIME stRightTime;//按日期
	char sRightDataDesc[ICMS_COMM_LEN];//详细参数
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_ROLE_RIGHT, *LPICMS_ROLE_RIGHT;
//语音对讲和云台占用返回用户信息
typedef struct
{
	char sUserName[ICMS_COMM_LEN];//用户名
	char sUserPhone[ICMS_COMM_LEN];//用户电话
	char sUserRegion[ICMS_COMM_LEN];//用户区域组织名称
}ICMS_HOLD_USER,*LPICMS_HOLD_USER;
//用户信息
typedef struct
{
	long   lUserId;
	char   sUserLoginName[ICMS_NAME_LEN];      //用户登陆名
	char   sUserName[ICMS_NAME_LEN];   //用户名称
	char   sUserPassword[ICMS_COMM_LEN];//用户密码
	char   sUserDsc[ICMS_NAME_LEN];    //用户描述
	char   sUserRight[ICMS_NAME_LEN];//用户权限，以“，”间隔，目前未用
	char   sUserPhone[ICMS_COMM_LEN];//用户电话
	char   sUserEmail[ICMS_COMM_LEN];//用户email
	BYTE   bUserType;       //用户类型  用户类型（1-管理员、2-上级平台、3-自定义）
	long   lRoleId;        //所属角色id
	ICMS_ROLE_RIGHT  stRoleRight[ICMS_COMM_LEN];//目前是38个
	BYTE   bReRoleRightCount;//实际权限个数
	BYTE   bReserved[ICMS_COMM_LEN];    //保留
} ICMS_USERINFO,*LPICMS_USERINFO;//每个用户组最多多少个用户，64？

//用户组信息
typedef struct
{
	long   lUserGroupId;     //用户组id
	char   sRoleName[ICMS_NAME_LEN];   //名字
	char   sRoleDsc[ICMS_NAME_LEN];     //描述
	char   sRoleRight[ICMS_NAME_LEN];    //以“，”间隔，目前未用
	long   lRegionId;      //所属区域id
	BYTE   bRoleType;//角色类型（0-厂家维护管理员、1-用户系统管理员2-上级平台、3-自定义）
	ICMS_ROLE_RIGHT  stRoleRight[ICMS_COMM_LEN];//目前是38个
	BYTE   bReRoleRightCount;//实际权限个数
	BYTE   bReserved[ICMS_COMM_LEN];            //保留
} ICMS_USERGROUPINFO,*LPICMS_USERGROUPINFO;

//实时流信息
typedef struct{
	char szPath[ICMS_NAME_LEN];			 //转发服务器路径
	LONG lChannel;						//通道号
	char byStreamType;                   //流类型 0-主码流，1-子码流
	HWND hPlayWnd;						//播放窗口的句柄,为NULL表示不播放图象
	long lPlayMode;                     //播放模式
	char cReserved[ICMS_COMM_LEN];      //保留
}ICMS_CLIENTINFO,*LPICMS_CLIENTINFO;

//历史流播放信息
typedef struct{
	char	szPath[ICMS_NAME_LEN];	//转发服务器路径
	DWORD	dwDevID;				//设备ID
	long    lChannel;				  //通道号
//	long 	lStorePath;            //存储路径 0-设备存储，1,-本级存储，2-级联平台存储
	char    sStorePath[ICMS_COMM_LEN]; //存储路径
	long	lFileType;				//录像类型
	long	lStreamType;			//流类型
	ICMS_TIME  stBgnTime; //开始时间
	ICMS_TIME  stEndTime; //结束时间
	HWND hPlayWnd;				 //播放窗口的句柄,为NULL表示不播放图象
	long lPlayMode;              //播放模式
	char sReserved[ICMS_COMM_LEN];
}ICMS_HIS_CLIENTINFO,*LPICMS_HIS_CLIENTINFO;
//历史视频请求列表信息
typedef struct{	
	long    lNodeType;//设备区域组织类型
	char    sDevPath[ICMS_NAME_LEN];//设备路径，一级平台为空，下级平台根据规则写入

	DWORD   dwDevID;//设备id
	long    lChanID;//通道id
//	long    lChanCount;//通道个数
	long    lStreamType;//流类型,0-主码流，1-子码流，为图片时是图片格式，0--jpg ，1-bmp ；目前只能是jpg 格式
	long    lStorePath;  //存储路径 0-设备存储，1,-本级存储，2-级联平台存储
// 	char    sFileName[128];  //文件名//文件名
	long    lFileType;      //录像类型
//	ULONGLONG   dwFileSize; //文件大小
	ICMS_TIME  stBgnTime; //开始时间
	ICMS_TIME  stEndTime; //结束时间
	long	lLogType;//HisLogType 0-录像  1-图片
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_HIS_VIDEO,*LPICMS_HIS_VIDEO;
//历史视频返回列表结构体
typedef struct
{
	char	sDevPath[ICMS_NAME_LEN];	//转发服务器路径
	long    lStreamType;             //流类型,0-主码流，1-子码流，为图片时是图片格式，0--jpg ，1-bmp ；目前只能是jpg 格式
	long 	lStorePath;            //存储路径标志 0-设备存储，1,-本级存储，2-级联平台存储
	char    sStorePath[ICMS_COMM_LEN]; //存储路径
	long    lChanID;              //通道id
	long    lLogType;    //视频or图片//HisLogType 0-录像  1-图片
	char    sFileName[ICMS_NAME_LEN];  //文件名
	ICMS_TIME   stBeginTime; //开始时间
	ICMS_TIME   stEndTime;//结束时间
	ULONGLONG   dwFileSize; //文件大小
	long      lFileType;	    //录像类型
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_QUERY_HIS,*LPICMS_QUERY_HIS;
//历史视频下载结构体
typedef struct  
{
	char    sDevPath[ICMS_COMM_LEN];//路径
	long    lChanID;//通道id
	long    lStreamType;//流类型
	char    sStorePath[ICMS_COMM_LEN];//存储路径,查询历史视频返回的数据
	long    lFileType;//录像类型
	ULONGLONG   dwFileSize;//文件大小
	ICMS_TIME  stBgnTime;//开始时间
	ICMS_TIME  stEndTime;//结束时间
	long	lLogType;//HisLogType 0-录像  1-图片
	char    sParentName[ICMS_NAME_LEN];//设备名称
	char    sChanName[ICMS_NAME_LEN];//通道名称
}ICMS_DOWNLOAD_HIS,*LPICMS_DOWNLOAD_HIS;
typedef struct  
{
	long lDiskId;//磁盘id
	char sDiskName[ICMS_COMM_LEN];//磁盘名称
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_DISK,*LPICMS_DISK;
//服务器信息
typedef struct
{
	DWORD dwServiceId;         //服务id
	char sServiceSession[ICMS_COMM_LEN];      //服务Session
	char sServiceName[ICMS_COMM_LEN];       //服务名称
	char sServiceDesc[ICMS_NAME_LEN];       //服务描述
	char sServiceIp[ICMS_COMM_LEN];          //服务ip
	DWORD dwServicePort;       //服务端口
	BYTE bServiceType;          //服务器类型
	BYTE bIsOnline;             //是否在线
	union SERVICE_TYPE         //占长度288字节，默认4字节对齐
	{
		//流媒体服务器。
		struct SERVICE_MEDIA
		{
			char  sServiceOutIP[ICMS_COMM_LEN];      //外网ip
			DWORD dwServiceOutPort;    //外网port
			BYTE  bServiceDefault;        //是否默认流媒体，是为1
			DWORD dwLimitRTStream;     //最大传输流
			char sReserve[212];
		} SerMedia;

		//存储服务器。
		struct SERVICE_STORE
		{ 
//			ICMS_DISK stDisk[ICMS_MAX_NUM];//新增的磁盘id为空
			char sServiceStorePath[ICMS_NAME_LEN];    //存储盘,多个盘间用：隔开，例如“D:E:F:G:”
			BYTE bStoreType;              //存储类型
			long lReDiskCount;           //实际盘符个数
			char sReserve[24];
		} SerStore;
		struct SERVICE_DECODE
		{
			long lCycleGroupId;//轮询方案id
			long lAlarmGroupId;//报警方案id
			long lRegionId;//区域id
			long lCyclePause;//未知？
			char sReserve[272];
		}SerDecode;
	}ser;
}ICMS_SERVICE,*LPICMS_SERVICE;

//存储设备状态
typedef struct  
{
    char sDevPath[ICMS_COMM_LEN];//设备路径
	long lChannId;//通道id
	bool bIsOnline;//是否在线
	char sChanName[ICMS_NAME_LEN];//通道名称
	char sDeviceName[ICMS_NAME_LEN];//设备名称
	long lRecordStauts;//存储录像状态，见枚举
	char sReserve[ICMS_COMM_LEN];
}ICMS_STORE_STATUS,*LPICMS_STORE_STATUS;

//窗口句柄
typedef struct  
{
	HWND hWnd[ICMS_COMM_LEN];
	long lReWnd;//实际个数
}ICMS_HWND,*LPICMS_HWND;

//窗口信息
typedef struct 
{
	DWORD dwParentId;//所属段id
	DWORD dwWndNum;//窗口序号
	long  lDecodeDeviceId;//解码设备id
	long  lDecodeDeviceChanId;//解码设备通道id
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_WND,*LPICMS_WND;
//轮询方案时间段信息
typedef struct
{
	DWORD dwCycleId;//段id
	char sBeginTime[ICMS_COMM_LEN];//开始时间
	char sEndTime[ICMS_COMM_LEN];//结束时间
	DWORD dwCycleInter;//轮询间隔
	DWORD dwResWndNum;//实际窗口个数
	ICMS_WND stWnd[ICMS_MAX_WND_NUM];
	DWORD  dwResChannNum;//实际通道个数
//	DWORD dwRoleId;//角色id，用户所属角色id-为通道轮询，0-为轮询和报警上墙
	BYTE  bEnable;//是否使用
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_CUSTOMCYCLE_INFO,*LPICMS_CUSTOMCYCLE_INFO;//最多64个
//轮询方案信息
typedef struct
{
	long  lCycleGroupId;//方案id
	char  sGroupName[ICMS_NAME_LEN];//方案名字
	DWORD dwCycleRow;  //行数
	DWORD dwCycleColumn;//列数
// 	DWORD dwRoleId;//角色id，用户所属角色id-为通道轮询，0-为轮询和报警上墙
// 	BYTE  bIsTv;//是否是电视墙
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_CYCLEGROUP_INFO,*LPICMS_CYCLEGROUP_INFO;

//时间段   -----------------------------------------时间段值全部为0表示无效时间段
typedef struct
{
	bool bEnable;
	long lAlarmGrade;//报警等级-低，中，高
	long lStartHour;
	long lStartMin;
	long lStartSec;
	long lEndHour;
	long lEndMin;
	long lEndSec;
}ICMS_ALARM_TIME_SEG, *LPICMS_ALARM_TIME_SEG;

//录像参数
typedef struct
{
	bool bRecord;													//是否录像 FALSE-否 TRUE-是
	long lPreRecTime;												//预录像时间
	ICMS_ALARM_TIME_SEG arrRecTimeSeg[ICMS_MAX_WEEK_DAY][ICMS_MAX_TIME_SEG];		//录像时间段
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_RECORD_TIME, *LPICMS_RECORD_TIME;

//存储方案信息
typedef struct  
{
	DWORD dwStrategyId;//方案id
	char  sStrategyName[ICMS_NAME_LEN];//方案名字
	BYTE  bStrategyType;//存储类型
	DWORD dwStoreDisk;//存储磁盘id
	char  sStoreServerName[ICMS_NAME_LEN];//存储服务器名称
	DWORD dwStoreServerId;//存储服务器id,增加方案不需写
	DWORD dwStoreDay;//存储天数
	DWORD dwStoreFileSize;//存储文件大小
	DWORD dwAlarmStoreTime;//报警存储时间--除去定时录像都需要设置此参数，若存储类型为历史备份则0-本地备份时间，1-远程录像记录
	ICMS_RECORD_TIME stRecordTime;//存储时间
	ICMS_DISK stDisk;//盘符--增加存储方案和修改时不需要赋值此参数
	BYTE  bStreamType;//码流类型
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_STORE_STRATEGY,*LPICMS_STORE_STRATEGY;
//报警关联输出
typedef struct
{
	long lAlarmSourceId;//报警来源id
	char sDevPath[ICMS_COMM_LEN];//设备路径，一级平台为空，下级平台根据规则写入
	long lChannId;//视频通道id
	char sRegionName[ICMS_NAME_LEN];//区域名称
	char sDevName[ICMS_NAME_LEN];//设备名称
	char sChannName[ICMS_NAME_LEN];//视频通道名称
	long lOutTime;//输出时间
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_ALARM_LINK_OUT,*LPICMS_ALARM_LINK_OUT;
//报警关联视频
typedef struct
{
	long lAlarmSourceId;//报警来源id
	char sDevPath[ICMS_COMM_LEN];//设备路径，一级平台为空，下级平台根据规则写入
	long lChannId;//视频通道id
	char sRegionName[ICMS_NAME_LEN];//区域名称
	char sDevName[ICMS_NAME_LEN];//设备名称
	char sChannName[ICMS_NAME_LEN];//视频通道名称
	long lPresetNo;//预置点
	long lPhotoCount;//拍照数
	long lPhotoInter;//拍照间隔
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_ALARM_LINK_VIDEO,*LPICMS_ALARM_LINK_VIDEO;
//报警来源
typedef struct  
{
	long lSchemeId;//报警方案id
	long lSourceId;//报警来源id
	long lNodeType;//类型
	char sDevPath[ICMS_COMM_LEN];//设备路径，一级平台为空，下级平台根据规则写入
//	long lDevId;//设备id
	long lDevChannId;//通道id
	BYTE  bDevType;//通道类型
	long lWndRow;//窗口行
	long lWndColumn;//窗口列
	long lCycleInter;//间隔
	char sRegionName[ICMS_NAME_LEN];//区域名称
	char sDevName[ICMS_NAME_LEN];//设备名称
	char sChanName[ICMS_NAME_LEN];//视频通道名称
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_ALARM_SOURCE,*LPICMS_ALARM_SOURCE;

//报警方案
typedef struct  
{
	long lSchemeId;//报警方案id
	char sSchemeName[ICMS_NAME_LEN];//方案名称
	char sAlarmType[ICMS_MAX_LEN];//报警类型，各个类型间加“，”隔开，前后都有“，”，例如“,1,2,3,1000,”
	long lWndRow;//窗口行
	long lWndColumn;//窗口列
	long lCycleInter;//轮询间隔
	long lStoreStrategyId;//存储策略id
//	BYTE bAlarmGrade;//报警等级-低，中，高
	long lAlarmLinkDecodeId;//报警上墙id
	long lEmailUserId;//发件人
	ICMS_RECORD_TIME stAlarmTime;//报警时间 
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_ALARM_SCHEME,*LPICMS_ALARM_SCHEME;

//关联报警视频
typedef struct  
{
	char sDevPath[ICMS_COMM_LEN];//设备路径，一级平台为空，下级平台根据规则写入
	long lDevId;//设备id
	long lChanId;//通道id
	long lStreamType;//流类型，0-录像流，1-网络流
	char sRegionName[ICMS_NAME_LEN];//区域名称
	char sDevName[ICMS_NAME_LEN];//设备名称
	char sChannName[ICMS_NAME_LEN];//通道名称
    BYTE bPlayDirect;//是否直连
    char sReserve[ICMS_COMM_LEN];//保留 
}ICMS_IMMEDIATE_ALARM_VIDEO,*LPICMS_IMMEDIATE_ALARM_VIDEO;

//节假日
typedef struct  
{
   long lHolidayId;//节假日id
   ICMS_TIME stTime;//只有年月日
   bool bIsEnable;//是否启用
   int  iWeek;//星期几,查看星期枚举
}ICMS_HOLIDAY,*LPICMS_HOLIDAY;
//报警信息上报
typedef struct
{
	long lAlarmType;//报警类型
	long lArarmLevel;//报警等级
	long lChanId;//通道id
	char sDevPath[ICMS_COMM_LEN];//设备路径，一级平台为空，下级平台根据规则写入
	long lDevId;//设备id
	char sSerial[ICMS_COMM_LEN];//报警序列号
	long lSignalStatus;//报警上传状态
	long lSignalId;//报警id？暂不需要
	ICMS_TIME stAlarmTime;//开始时间
//	ICMS_TIME stEndTime;//结束时间
	char sRegionName[ICMS_NAME_LEN];//区域名称
	char sDevName[ICMS_NAME_LEN];//设备名称
	char sChannName[ICMS_NAME_LEN];//通道名称
	BYTE bLinkVideo;//是否关联视频
	long lWndRow;//窗口行
	long lWndColumn;//窗口列
	long lTimeInter;//时间间隔
	long lReCount;//实际关联视频个数
    ICMS_IMMEDIATE_ALARM_VIDEO stImmAlarmVideo[64];
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_IMMEDIATE_ALARM,*LPICMS_IMMEDIATE_ALARM;
//查询天数
typedef struct
{
   int iDay[31];//天数数组，每一个值代表一个月中的某天
   int iReDay;//实际天数
}ICMS_QUERY_DAY,*LPICMS_QUERY_DAY;
typedef struct
{
   int iMonth[12];
   int iReMonth;
}ICMS_QUERY_MONTH,*LPICMS_QUERY_MONTH;
//请求报警管理
typedef struct
{
   long lNodeType;//区域设备组织类型
   ICMS_TIME stBeginTime;//开始时间
   ICMS_TIME stEndTime;//结束时间
   long lAlarmType[ICMS_COMM_LEN];//每一个代表一个报警类型
   long lAlarmTypeCount;//报警个数
//   long lAlarmLevel;//报警等级？？
//   char sPsPath[ICMS_COMM_LEN];//下级路径？？
   char sDevPath[ICMS_COMM_LEN];////设备路径，一级平台为空，下级平台根据规则写入
   bool bIsIncludeLow;//是否包括下级区域和分组
   long lId;//区域或者设备或者通道id
   char sCompany[ICMS_NAME_LEN];//企业名称
//   char sRegionName[ICMS_NAME_LEN]; //设备名称 
   bool bDealMark;//处理标记
   char sDealUser[ICMS_COMM_LEN];//处理用户
   char sReserve[ICMS_COMM_LEN];//保留
}ICMS_ALARM_MNG,*LPICMS_ALARM_MNG;
//返回报警管理
typedef struct
{
	char sSerial[ICMS_NAME_LEN];//报警序列号
	long lAlarmid;//报警id
	char sAlarmBeginTime[ICMS_COMM_LEN];//开始时间
	char sAlarmEndTime[ICMS_COMM_LEN];//结束时间
	long lAlarmType;//报警类型
	long lAlarmLevel;//报警等级
	char sDevPath[ICMS_COMM_LEN];//设备路径，为“:设备id”，例如设备id为27，则为“:27”
	long lDevChan;//设备通道id
	long lRegionId;//区域id
	char sCompany[ICMS_NAME_LEN];//企业名称
	char sDevName[ICMS_NAME_LEN];//设备名称
	char sChanName[ICMS_NAME_LEN];//通道名称
	char sRegionName[ICMS_NAME_LEN];//区域名称
	char sDealMsg[ICMS_NAME_LEN];//处理信息
	bool bDealMark;//处理标记
	char sDealTime[ICMS_COMM_LEN];//处理时间
	char sDealUser[ICMS_COMM_LEN];//处理用户
    char sReserve[ICMS_COMM_LEN];//保留
}ICMS_ALARM_MNG_RE,*LPICMS_ALARM_MNG_RE;

//请求系统日志管理
typedef struct
{
	ICMS_TIME stBeginTime;//开始时间
	ICMS_TIME stEndTime;//结束时间
	char sPsPath[ICMS_COMM_LEN];//下级路径
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_LOG_MNG,*LPICMS_LOG_MNG;
//返回日志管理
typedef struct
{
	long lLogType;//日志类型
	char sLogTime[ICMS_COMM_LEN];//日志时间
	long lLogCode;//日志代码
	char sLogSubject[ICMS_NAME_LEN];//操作主体
	long lSubjectType;//对象类型
	char sLogObject[ICMS_NAME_LEN];//操作客体
	long lObjectType;//对象类型
	long lLogErr;//错误码
    char sReserve[ICMS_COMM_LEN];//保留
}ICMS_LOG_MNG_RE,*LPICMS_LOG_MNG_RE;
//代理服务器
typedef struct  
{
	long lProxyId;//代理服务器id
	char sProxyName[ICMS_COMM_LEN];//代理服务器名称
	bool bIsOnline;//是否在线
	char sProxyIp[ICMS_COMM_LEN];//ip
	long lPort;//端口
}ICMS_PROXY_SERVER,*LPICMS_PROXY_SERVER;
//级联平台
typedef struct
{
	long lProxyId;//代理服务器id
	long lServerId;//服务器id
	long lRegionId;//区域id
	char sRegionName[ICMS_COMM_LEN];//区域名称
    char sRegisterId[ICMS_NAME_LEN];//注册id
	char sSerial[ICMS_NAME_LEN];//序列号
	char sPsName[ICMS_COMM_LEN];//名称
	char sPsDesc[ICMS_NAME_LEN];//描述
    long lConnErr;//错误号
	char sIp[ICMS_COMM_LEN];//ip
	long lPort;//端口
	char sUserName[ICMS_MAX_NUM];//用户名
	char sPassWord[ICMS_MAX_NUM];//密码
	bool bIsOnLine;//在线状态
	bool bIsLocalCtrl;//是否允许本级用户直连
	bool bIsQtherCtrl;//是否允许其他平台直连
	BYTE bConnectType;//连接类型，1为发起连接，0为接入连接
	char sShareFrom[ICMS_MAX_LEN];//获取共享资源，多项用"，"隔开,例如“,1,3,2,402,”
	char sShareTo[ICMS_MAX_LEN];//本级共享，多项用"，"隔开，例如“,1,3,2,402,”

}ICMS_PS_SERVER,*LPICMS_PS_SERVER;

//级联平台定制（内蒙电力）
typedef struct
{
//	long lServerId;//服务器id
	char sOuoppKeyId[ICMS_COMM_LEN];//主键编号
	char sOuoppName[ICMS_COMM_LEN];//名称
	char sOrgKeyId[ICMS_COMM_LEN];//组织机构编码
	char sRemoteOrgKeyId[ICMS_COMM_LEN];//级联服务器组织机构编码
//	long lConnErr;//错误号
	char sIp[ICMS_COMM_LEN];//ip
	long lPort;//端口
//	bool bIsOnLine;//在线状态
}ICMS_PS_SERVER_SPEC, *LPICMS_PS_SERVER_SPEC;
//解码设备通道配置
typedef struct
{
	long lDeviceId;//设备id
	long lChannId;//通道id
	long lOutType;//解码输出模式,见ModeOutType
	long lMatrixCount;//窗口总数
	long lLeftPos;//窗口左位置
	long lTopPos;//窗口顶位置
	long lRightPos;//窗口右位置
	long lBottomPos;//窗口底位置
}ICMS_DECODEDEVICE_CHANN,*LPICMS_DECODEDEVICE_CHANN;
//解码设备类型
typedef struct 
{
    long lDecodeTypeId;//解码设备类型id
	char sDecodeTypeName[ICMS_COMM_LEN];//解码设备类型名称
}ICMS_DECODEDEVICE_TYPE,*LPICMS_DECODEDEVICE_TYPE;
//解码设备
typedef struct  
{
   long lDeviceId;//设备id
   long lServerId;//解码服务器id
   char sServerName[ICMS_COMM_LEN];//解码服务器名称
   char sDeviceName[ICMS_COMM_LEN];//设备名字
   long lDeviceType;//解码设备类型，见上枚举dev_type
//   char sDeviceTypeName[ICMS_COMM_LEN];//设备类型名称
   char sDeviceAddr[ICMS_COMM_LEN];//设备地址
   long lDevicePort;//端口
   char sUserName[ICMS_COMM_LEN];//用户名
   char sPassWord[ICMS_COMM_LEN];//密码
   long lRegionId;//区域id
   long lChanCount; //每个硬件通道个数
   long lHardWareCount;//硬件个数，解码卡等
   long lCycleGroupId;//轮询方案id,初始为0
   long lCycleStream;//默认为1
   long lAudioType;//音频输出接口类型
   long lCycleAsync;//默认为0
   long lConError;  //连接状态
   long lReCount;  //实际通道个数
   ICMS_DECODEDEVICE_CHANN stChann[ICMS_COMM_LEN];
}ICMS_DECODEDEVICE,*LPICMS_DECODEDEVICE;
//状态信息
typedef struct  
{
	DWORD   dwDeviceID;//设备id
	char    sDeviceName[ICMS_NAME_LEN];//设备名称
	bool    bOnline;//是否在线
	long    nStatusCount;//状态个数
	bool    bStatus[ICMS_COMM_LEN];//每个数组代表一个状态
}ICMS_STATUS_SIGNAL,*LPICMS_STATUS_SIGNAL;

//设备磁盘状态
typedef struct  
{
	DWORD dwTotalSpace;//总空间,单位为MB
	DWORD dwFreeSpace;//剩余空间
//	DWORD dwDiskState;//磁盘状态
}ICMS_DEVICE_DISK,*LPICMS_DEVICE_DISK;
//温湿度状态
typedef struct  
{
	long lDeviceId;//设备id
}ICMS_DEVICE_HUMITURE,*LPIICMS_DEVICE_HUMITURE;

//设备节点
typedef struct
{
	long lLayerId;//图层id
	ICMS_DLData stDlData;
	long lTextColor;//字体颜色
	long lTextSize;//文字大小
	long lPosX;//x坐标
	long lPosY;//y坐标
}ICMS_DEVICE_NODE,*LPICMS_DEVICE_NODE;

//图层节点
typedef struct
{
	long lParentId;//上级图层id
	long lLayerId;//图层id
	char sName[ICMS_COMM_LEN];//图层名称
	long lTextColor;//字体颜色
	long lTextSize;//文字大小
	long lPosX;//x坐标
	long lPosY;//y坐标
}ICMS_LAYER_NODE,*LPICMS_LAYER_NODE;

//电子地图图层
typedef struct  
{
	long lParentId;//图层父id
	long lLayerId;//图层id
	long lMapType;//图层图片类型
	char sMapName[ICMS_COMM_LEN];//图层图片名称
}ICMS_MAP,*LPICMS_MAP;

//人脸识别照片信息
typedef struct
{
	char sPeopleId[ICMS_COMM_LEN];//人员id
	long lPicId;//照片id
	char slibId[ICMS_COMM_LEN];//人脸库id	
	long lPicLen;//人脸照片长度
	char psPeopleFacePic[ICMS_PIC_MAX_LEN];//人脸照片,base64编码数据
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_FACE_PIC,*LPICMS_FACE_PIC;

//人脸识别报警查询
typedef struct
{
	ICMS_TIME stAlarmBeginTime;//报警开始时间
	ICMS_TIME stAlarmEndTime;//报警结束时间
	ICMS_TIME stHandlBeginTime;//报警处理开始时间
	ICMS_TIME stHandlEndTime;//报警处理结束时间
	long lDeviceId;//设备Id
	char sPeopleId[ICMS_COMM_LEN];//人员id
	long lSimilarMin;//相似度最小值
	long lSimilarMax;//相似度最大值
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_FACE_ALARM,*LPICMS_FACE_ALARM;

//返回人脸识别报警信息
typedef struct
{
	char  sLogId[ICMS_COMM_LEN];//报警日志id
	char  sPeopleId[ICMS_COMM_LEN];//人员id
	char  sSnapTime[ICMS_COMM_LEN];//抓拍时间
	long  lPicId;//图片id，实时上报没有此id
	long lDeviceId;//设备Id
	long lHandleState;//处理状态
	long lAlarmType;//报警类型  0：抓拍报警 1：识别报警
	char sHandleMsg[ICMS_NAME_LEN];//处理结果
	long lSimilarDegree;//相似度
	long lSnapPicLen;//抓拍照片长度
	long lPeopleFacePicLen;//人脸照片长度
	char psSnapPic[ICMS_PIC_MAX_LEN];//抓拍照片,base64编码数据
	char psPeopleFacePic[ICMS_PIC_MAX_LEN];//人脸照片,base64编码数据	
	char sReserve[ICMS_COMM_LEN];//保留
}ICMS_FACE_ALARM_RE,*LPICMS_FACE_ALARM_RE;

//虚拟用户结构体
typedef struct
{
	char sUserName[ICMS_COMM_LEN];//编号
	char sUserPass[ICMS_COMM_LEN];//用户名密码
//	char sSrcServerKeyId[ICMS_COMM_LEN];//源用户管理服务器keyid
//	char sSrcServerIp[ICMS_COMM_LEN];//源用户管理服务器ip
//	DWORD dwSrcServerPort;//源用户管理服务器port
	bool bIsNeverFailure;//用户长期有限标志
	ICMS_TIME stUseBeginTime;//用户临时生效时间
	ICMS_TIME stUseEndTime;//用户临时失效时间
	bool bIsAutoClear;//临时失效后自动清理
	bool bIsSystem;//系统预留标记
	bool bIsDel;//上锁免删除
	bool bIsDisable;//禁用标志
	char sRemark[ICMS_NAME_LEN];//备注
	char sMd5[ICMS_COMM_LEN];//更新验证码
}ICMS_VIRTUAL_USER,*LPICMS_VIRTUAL_USER;

//岗位角色信息
typedef struct
{
	long lPostKeyId;//编号
	char sPostName[ICMS_COMM_LEN];//名称
}ICMS_POST_INFO,*LPICMS_POST_INFO;

//组织机构和角色
typedef struct
{
	char    sPsPath[ICMS_COMM_LEN];//级联路径
	char    sKeyId[ICMS_COMM_LEN];   //角色或者组织机构或者级联服务器keyid
	char    sParentKeyId[ICMS_COMM_LEN];   //角色或者组织机构或者级联服务器的父keyid
	char    sName[ICMS_COMM_LEN];//名称	
	long    lType;//组织机构角色类型，0-未知，1-组织机构，2-角色，3-级联服务器
	bool    bIsBind;//是否已绑定角色
}ICMS_ORG_ROLE, *LPICMS_ORG_ROLE;
//#pragma pack()